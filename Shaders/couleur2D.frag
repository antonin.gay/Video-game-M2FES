// Version
#version 150 core

// Entrées
in vec4 color;

// Sorties
out vec4 out_Color;

// Uniformes
uniform vec3 maCouleur;

// Fonction main
void main()
{
    // Recopie et inverse la couleur
    out_Color = vec4(maCouleur, 1.0);
}