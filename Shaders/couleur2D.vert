// Version
#version 150 core

// Entrées
in vec2 in_Vertex;
in vec4 in_Color;

// Sorties
out vec4 color;

// Uniform
uniform mat4 modelView;
uniform mat4 projection;

// Foncrion main
void main()
{
    // Position finale du vertex
    gl_Position = vec4(in_Vertex, 0.0, 1.0);

    // définition de ma couleur
    vec4 maCouleur = (in_Vertex.x >0) ?
        vec4(1.0, 0.0, 0.0, 1.0) : vec4(0.0, 0.0, 1.0, 1.0);

    // Envoi de la couleur au fragment
    color = maCouleur;

    // Multiplication modelView et projection
    mat4 multiplication = modelView * projection;
}