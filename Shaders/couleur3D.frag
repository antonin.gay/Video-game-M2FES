// Version
#version 150 core

// Entrées
in vec3 color;

// Sorties
out vec4 out_Color;

// Fonction main
void main()
{
    // Recopie et inverse la couleur
    out_Color = vec4(color, 1.0);
}