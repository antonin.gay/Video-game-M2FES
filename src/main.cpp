#define SDL_MAIN_HANDLED

#include <iostream>
#include "../Class/Scene3D.h"
#include "../Class/Objets3D/CarreCoul.h"

// TODO : modifier UML

int main(int argc, char **argv) {
    std::cout << "Hello, World!" << std::endl;

    // Création de la scène
    Scene3D scene3D("Test_JV", 800, 600);

    // Initialisation de la scene
    if(!scene3D.initFenetre())
        return -1;

    if(!scene3D.initOpenGL())
        return -1;

    // Ajout d'objets 3D
    // Cubes texturés
//    CubeText *cube1 = new CubeText(vec3(1, 0, 0), 0, "../Shaders/texture.vert", "../Shaders/texture.frag", 1,
//                                   "../Assets/crate07_2b.jpg");
//    scene3D.ajouterObjet3D(cube1);
//
//    cube1->tourner(-30);
//    cube1->deplacer(vec3(0.5, 0, 0));
//
//    scene3D.ajouterObjet3D(
//            new CubeText(vec3(1, 1, 0), 45, "../Shaders/texture.vert", "../Shaders/texture.frag", 1,
//                        "../Assets/crate07_2b.jpg"));
//    scene3D.ajouterObjet3D(
//            new CubeText(vec3(1, 2, 0), 70, "../Shaders/texture.vert", "../Shaders/texture.frag", 1,
//                        "../Assets/crate07_2b.jpg"));
//
//    // Cubes Couleurs
//    scene3D.ajouterObjet3D(
//            new CubeCoul(vec3(-3, 0, 0), 45, "../Shaders/couleur3D.vert", "../Shaders/couleur3D.frag", 1,
//                         0, 9 * 16 + 8, 8 * 16 + 8));
//    scene3D.ajouterObjet3D(
//            new CubeCoul(vec3(-1, 1, 0), 80, "../Shaders/couleur3D.vert", "../Shaders/couleur3D.frag", 1,
//                         255, 9 * 16 + 8, 0));

    // Ciel et sol

    CubeCoul *ciel = new CubeCoul(vec3(16, 0, 16), 0, "../Shaders/couleur3D.vert", "../Shaders/couleur3D.frag", 50,
                                  9 * 16, 12 * 16 + 10, 15 * 16 + 9, false);
    ciel->setInverserCollisions(true);

    scene3D.ajouterObjet3D(ciel);

    scene3D.ajouterObjet3D(
            new CarreText(vec3(16, -0.5, 16), 0, "../Shaders/texture.vert", "../Shaders/texture.frag", 50,
                         "../Assets/veg011.jpg", 30));

    // On crée le personnage

    scene3D.setPersonnage(new Personnage(0.9, vec3(3, 0, 3), vec3(0, -1, 1)));

    // On crée la carte

    scene3D.chargerObjetsImagePNG("../Assets/carte1_CPC.png");

    // Boucle principale
    scene3D.bouclePrincipale();

    return 0;
}