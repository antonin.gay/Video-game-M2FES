//
// Created by Antonin on 02/05/2018.
//

#include "Scene3D.h"

Scene3D::Scene3D(std::string const titreFenetre, int const largeurFenetre, int const hauteurFenetre) :
        camera(vec3(3, 3, 3), vec3(0, 0, 0), vec3(0, 1, 0), 0.25f, 0.1f),
        input(), personnage(), vectorObjets3D(), titreFenetre(titreFenetre),
        largeurFenetre(largeurFenetre), hauteurFenetre(hauteurFenetre),
        fenetre(nullptr), contexteOpenGL(nullptr) {}

Scene3D::~Scene3D() {
    for(Objet3D *objet3D : vectorObjets3D)
        delete objet3D;

    delete personnage;

    SDL_GL_DeleteContext(contexteOpenGL);
    SDL_DestroyWindow(fenetre);
    SDL_Quit();
}

bool Scene3D::initFenetre()
{
    // Initialisation de la SDL

    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << std::endl;
        SDL_Quit();

        return false;
    }

    // Version d'OpenGL
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // Double Buffer
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);


    // Création de la fenêtre

    fenetre = SDL_CreateWindow(titreFenetre.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, largeurFenetre, hauteurFenetre, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL); // NOLINT

    if(fenetre == nullptr)
    {
        std::cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << std::endl;
        SDL_Quit();

        return false;
    }


    // Création du contexte OpenGL

    contexteOpenGL = SDL_GL_CreateContext(fenetre);

    if(contexteOpenGL == nullptr)
    {
        std::cout << SDL_GetError() << std::endl;
        SDL_DestroyWindow(fenetre);
        SDL_Quit();

        return false;
    }

    return true;
}

bool Scene3D::initOpenGL()
{
#ifdef WIN32

    // On initialise GLEW si sous Windows
    GLenum initialisationGLEW( glewInit() );

    // Si l'initialisation a échoué :

    if(initialisationGLEW != GLEW_OK)
    {
        // On affiche l'erreur grâce à la fonction : glewGetErrorString(GLenum code)

        std::cout << "Erreur d'initialisation de GLEW : " << glewGetErrorString(initialisationGLEW) << std::endl;


        // On quitte la SDL

        SDL_GL_DeleteContext(contexteOpenGL);
        SDL_DestroyWindow(fenetre);
        SDL_Quit();

        return false;
    }

#endif

    // Activation du Depth Buffer

    glEnable(GL_DEPTH_TEST);

    // Tout s'est bien passé, on retourne true

    return true;
}

void Scene3D::bouclePrincipale() {
    // Variables
    unsigned int framerate(16); // 16ms pour 60 i/s
    Uint32 debutBoucle(0), finBoucle(0), tempsEcoule(0);

    // Initialisation des matrices
    mat4 projection = perspective(70.0, (double) largeurFenetre / hauteurFenetre, 1.0, 100.0);
    mat4 modelview = mat4(1.0);

    // Cacher pointeur
    input.afficherPointeur(false);
    input.capturerPointeur(true);

    // ===== Création d'objets ici éventuellement pour tester =====

    // ===== Chargement des objets créés =====

    // ===== Fin de la création d'objets ======

    while (!input.terminer())
    {
        // On enregistre le temps en début de boucle
        debutBoucle = SDL_GetTicks();

        // ===== Gestion des évènements =====
        input.updateEvenements();
        if(input.getTouche(SDL_SCANCODE_ESCAPE) || input.terminer())
            break;

        // ===== Gestion des déplacements =====
        deplacerPersonnage();
        orienterPersonnage();
        deplacerCamera();

        // ===== Affichage des objets =====
        afficherObjets(projection, modelview);

        // ===== Gestion framerate =====
        // Calcul du temps écoulé
        finBoucle = SDL_GetTicks();
        tempsEcoule = finBoucle - debutBoucle;

        // Si nécessaire, on met en pause le programme
        if(tempsEcoule < framerate)
            SDL_Delay(framerate - tempsEcoule);
    }
}

void Scene3D::afficherObjets(mat4 &projection, mat4 &modelview)
{
    // Nettoyage de l'écran
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Placement de la caméra
    camera.lookAt(modelview);

    // On appelle la fonction "afficher de tous les objets 3D présents
    for(Objet3D *objet3D : vectorObjets3D)
        objet3D->afficher(projection, modelview);

    // On affiche le personnage
    personnage->afficher(projection, modelview);

    // Actualisation de la fenêtre
    SDL_GL_SwapWindow(fenetre);
}

void Scene3D::ajouterObjet3D(Objet3D *objet3D) {
    // On ajoute un objet à la liste des objets
    vectorObjets3D.push_back(objet3D);
    objet3D->charger();
}

void Scene3D::setPersonnage(Personnage *personnage1)
{
    // On définit simplement le personnage
    personnage = personnage1;

    // On place la camera derrière le personnage
    deplacerCamera();
    camera.setPointCible(personnage->getPosition());
    camera.orienter(input.getM_xRel(), input.getM_yRel());
}

void Scene3D::deplacerPersonnage() {
    // On crée un vecteur de déplacement, qui sera remplit en fonction de la touche appuyée
    vec3 deplacement(0, 0, 0);

    // Si on avance ou recule, on va dans la direction d'orientation du personnage
    if(input.getTouche(SDL_SCANCODE_UP))
        deplacement = personnage->getOrientation();
    if (input.getTouche(SDL_SCANCODE_DOWN))
        deplacement = -1.0f * personnage->getOrientation();
    if (input.getTouche(SDL_SCANCODE_RIGHT))
        deplacement = personnage->getOrientationDroite();
    if (input.getTouche(SDL_SCANCODE_LEFT))
        deplacement = -1.0f * personnage->getOrientationDroite();

    deplacement.y = 0;

    // Si le déplacement n'est pas nul, on teste les collisions et on effectue le déplacement
    if(deplacement != vec3(0, 0, 0))
    {
        // On normalise le déplacement
        deplacement = camera.getVitesse() * normalize(deplacement);

        // On vérifie les collisions entre la nouvelle position et les objets de la scène
        vec3 newPosition(personnage->getPosition() + deplacement);
        float rayonCollision(0.2);
        float hauteurCollision(0.45);

        bool collision(false);

        // TODO : ne bloquer le déplacement que dans la direction incriminée
        // Pour chaque objet présent, on check les collisions
        for (auto objet : vectorObjets3D)
        {
            // Si il y a collision, on vérifie si il y a collision avec la nouvelle position
            if (objet->checkCollision(newPosition, rayonCollision, hauteurCollision, rayonCollision))
                // Si oui, on vérifie si il y avait avec l'ancienne
                if(!objet->checkCollision(personnage->getPosition(), rayonCollision, hauteurCollision, rayonCollision))
                {
                    // Si non, on ne doit alors pas avancer
                    collision = gestionCollision(deplacement, objet, rayonCollision);
                    if (collision)
                        break;
               }
        }

        if(!collision)
        {
            personnage->deplacer(deplacement);

            // On redéfinit alors le point cible de la caméra
            camera.setPointCible(personnage->getPosition());
        }
    }
}

void Scene3D::orienterPersonnage()
{

    if(input.mouvementSouris())
        camera.orienter(input.getM_xRel(), input.getM_yRel());

    personnage->setOrientation(camera.getOrientation());
}

void Scene3D::deplacerCamera()
{
    // On change la position de la caméra pour qu'elle se mette derrière le personnage
    camera.setPosition(personnage->getPosition() - 4.0f * personnage->getOrientation());
}

bool Scene3D::gestionCollision(vec3 &deplacement, Objet3D const *objetCollision, float const rayonCollision)
{
    // On rentre dans cette fonction si il y a collision entre l'objet et le personnage, afin d'adapter le vecteur de
    // déplacement. Si celui-ci ne peut pas être adapté, on signale, via le bool, une collision nette.

    // TODO : implémenter une "glissade" contre la surface incriminée
    // Il s'agirait de trouver la normale à la surface sur laquelle on butte et de se déplacer contre cette surface
    // Doit pouvoir se faire à partir de l'angle du cube non :o !

    return (true);
}

bool Scene3D::chargerObjetsImagePNG(const char *filePath)
{
    /*
     * Cette fonction charge une image, et crée la carte correspondante :
     * Chaque pixel correspond à un mètre carré de l'espace, si il est :
     * blanc c'est un mur,
     * noir rien,
     * bleu et rouge des cubes de couleurs
     */

    // On charge l'image
    SDL_Surface *carte = IMG_Load(filePath);

    // On va ensuite lire les pixels
    const int height = carte->h;
    const int width = carte->w;
    const int pitch = carte->pitch;
    auto *pixelsList = (Uint8 *) carte->pixels;

    // Les variables qui contiendront les couleurs
    Uint8 red;
    Uint8 green;
    Uint8 blue;

    // On verrouille l'image
    SDL_LockSurface(carte);


    for (int x = 0; x < height; x++){
        for (int z = 0; z < width; z++){

            // On se balade dans la liste des valeurs de pixel :
            // A chaque fois, on avance d'un pixel, qui correspond à 3 bytes (pitch/idth),
            // puisque pitch est le nombre de Bytes par ligne
            int index = (x * width + z ) * (pitch / width);

            red = pixelsList[index];
            green = pixelsList[index + 1];
            blue = pixelsList[index + 2];

            // On teste les couleurs
            if(red == 255 && green == 255 && blue == 255){
                // On ajoute un cube au niveau du sol
                this->ajouterObjet3D(
                        new CubeText(vec3(x, 0, z), 0, "../Shaders/texture.vert",
                                     "../Shaders/texture.frag", 1,
                                     "../Assets/crate07_2b.jpg"));

/*
                // On en ajoute un un mètre plus haut
                this->ajouterObjet3D(
                        new CubeText(vec3(x, 1, z), 0, "../Shaders/texture.vert",
                                     "../Shaders/texture.frag", 1,
                                     "../Assets/crate07_2b.jpg"));
*/
            } else if (red == 255){
/*
                // On ajoute deux cubes rouges
                this->ajouterObjet3D(
                        new CubeCoul(vec3(x, 0, z), 0, "../Shaders/couleur3D.vert",
                                     "../Shaders/couleur3D.frag", 1,
                                     255, 0, 0));

                // On ajoute le second, un mètre au-dessus
                this->ajouterObjet3D(
                        new CubeCoul(vec3(x, 1, z), 0, "../Shaders/couleur3D.vert",
                                     "../Shaders/couleur3D.frag", 1,
                                     255, 0, 0));
*/
            } else if (blue == 255){
                // On ajoute deux cubes rouges
                this->ajouterObjet3D(
                        new CubeCoul(vec3(x, 0, z), 0, "../Shaders/couleur3D.vert",
                                     "../Shaders/couleur3D.frag", 1,
                                     0, 0, 255));

/*
                // On ajoute le second, un mètre au-dessus
                this->ajouterObjet3D(
                        new CubeCoul(vec3(x, 1, z), 0, "../Shaders/couleur3D.vert",
                                     "../Shaders/couleur3D.frag", 1,
                                     0, 0, 255));
*/
            }
        }

        // On signale l'avancée
        std::cout << "Chargement de la carte : " << (100 * x) / 32 << " % " << std::endl;
    }

    SDL_UnlockSurface(carte);


    return false;
}
