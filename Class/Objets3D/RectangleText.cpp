//
// Created by mbauw on 5/14/18.
//

#include "RectangleText.h"

RectangleText::RectangleText(vec3 position, float angle, const std::string &vertexShader,
                             const std::string &fragmentShader, float longueur, float largeur,
                             const std::string &texturePath) :
        Rectangle(position, angle, vertexShader, fragmentShader, longueur, largeur, 0, 12)
{
    // on crée la texture
    texture = Texture(texturePath);
    texture.charger();

    // coordonnées de texture temporaires
    float coordTextureTmp[] = {1, 1, 1, 0, 0, 1,    // face 1
                               0, 0, 0, 1, 1, 0};   // face 1*

    // recopie des coordonnées
    for (int i(0); i < 12; i++)
        coordTexture[i] = coordTextureTmp[i];
}

RectangleText::RectangleText(vec3 position, float angle, const std::string &vertexShader,
                             const std::string &fragmentShader, float longueur, float largeur,
                             const std::string &texturePath, int nbRepetitionText) :
            RectangleText(position, angle, vertexShader, fragmentShader, longueur, largeur, texturePath)
{
    // on cast en float le nb de répétitions
    float nbRepet = nbRepetitionText;

    // coordonnées de texture temporaires, telles que la texture se répète
    float coordTextureTmp[] = {nbRepet, nbRepet, nbRepet, 0, 0, nbRepet,   // face 1
                                0, 0, 0, nbRepet, nbRepet, 0};        // face 1

    // recopie des coordonnées
    for (int i(0); i < 12; i++)
        coordTexture[i] = coordTextureTmp[i];
}

void RectangleText::envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) {
    // verrouillage de la texture
    glBindTexture(GL_TEXTURE_2D, texture.getID());

    // envoi des matrices
    shader.envoyerMat4("modelviewProjection", projection * modelview);

    // rendu
    glDrawArrays(GL_TRIANGLES, 0, nbVertices);

    // déverrouillage de la texture
    glBindTexture(GL_TEXTURE_2D, 0);
}