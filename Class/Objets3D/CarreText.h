//
// Created by Antonin on 03/05/2018.
//

/*
 * Un carré avec une texture, qu'on peut recopier de nombreuses fois sur l'objet
 */

#ifndef VIDEO_GAME_M2FES_CARRETEXT_H
#define VIDEO_GAME_M2FES_CARRETEXT_H


#include "Carre.h"
#include "Texture.h"

class CarreText : public Carre {

public:
    // Le constructeur classique : la texture prend tout l'objet
    CarreText(vec3 position, float angle, const std::string &vertexShader, const std::string &fragmentShader,
             float taille, const std::string &texturePath);

    // Le constructeur de texture pavée : on dit en paramètre le nombre de fois que la texture est répétée
    CarreText(vec3 position, float angle, const std::string &vertexShader, const std::string &fragmentShader,
              float taille, const std::string &texturePath, int nbRepetitionText);

private:

    void envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) override;

protected:
    Texture texture;

};


#endif //VIDEO_GAME_M2FES_CARRETEXT_H
