//
// Created by Antonin on 03/05/2018.
//

#include "CarreText.h"

CarreText::CarreText(vec3 position, float angle, const std::string &vertexShader,
                     const std::string &fragmentShader, float taille, const std::string &texturePath) :
        Carre(position, angle, vertexShader, fragmentShader, taille, 0, 12)
{
    // On crée la texture
    texture = Texture(texturePath);
    texture.charger();

    // Coordonnées de texture temporaires
    float coordTextureTmp[] = {1, 1, 1, 0, 0, 1,     // Face 1
                               0, 0, 0, 1, 1, 0};    // Face 1

    // Recopie des coordonnées
    for (int i(0); i < 12; i++)
        coordTexture[i] = coordTextureTmp[i];

}

CarreText::CarreText(vec3 position, float angle, const std::string &vertexShader, const std::string &fragmentShader,
                     float taille, const std::string &texturePath, int nbRepetitionText) :
    CarreText(position, angle, vertexShader, fragmentShader, taille, texturePath)
{
    // On cast en float le nb de répétitions
    float nbRepet = nbRepetitionText;

    // Coordonnées de texture temporaires, telles que la texture se répète
    float coordTextureTmp[] = {nbRepet, nbRepet, nbRepet, 0, 0, nbRepet,     // Face 1
                               0, 0, 0, nbRepet, nbRepet, 0};    // Face 1

    // Recopie des coordonées
    for (int i(0); i < 12; i++)
        coordTexture[i] = coordTextureTmp[i];

}

void CarreText::envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) {
    // Verrouillage de la texture
    glBindTexture(GL_TEXTURE_2D, texture.getID());

    // Envoi des matrices
    shader.envoyerMat4("modelviewProjection", projection * modelview);

    // Rendu
    glDrawArrays(GL_TRIANGLES, 0, nbVertices);

    // Déverrouillage de la texture
    glBindTexture(GL_TEXTURE_2D, 0);
}

