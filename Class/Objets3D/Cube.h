//
// Created by Antonin on 02/05/2018.
//

/*
 * La classe définissant un cube simple, sans texture ni couleur.
 * On a donc juste les vertices et le placement.
 */

#ifndef VIDEO_GAME_M2FES_CUBE_H
#define VIDEO_GAME_M2FES_CUBE_H

#include "Objet3D.h"

class Cube : public Objet3D{

public:
    Cube();

    Cube(vec3 position, float angle, const std::string &vertexShader, const std::string &fragmentShader, float taille,
         int tailleTabCoul, int tailleTabTexture);

    bool checkCollision(const vec3 &positionOther, const float &rayon) const override;

    bool checkCollision(const vec3 &position, const float &tailleOther, const float &angle)const  override;

    bool checkCollision(const vec3 &position, const float &tailleX, const float &tailleY,
                        const float &tailleZ) const override;

protected :
    float taille;
};


#endif //VIDEO_GAME_M2FES_CUBE_H
