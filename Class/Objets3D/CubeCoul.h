//
// Created by Antonin on 04/05/2018.
//

/*
 * Un cube dont les faces sont juste colorées et non texturées.
 * Elle hérite directement de Cube et prend en argument 3 couleurs RGB.
 */
#ifndef VIDEO_GAME_M2FES_CUBECOUL_H
#define VIDEO_GAME_M2FES_CUBECOUL_H

#include "Cube.h"
#include <cmath>

class CubeCoul : public Cube {

public:
    CubeCoul();

    CubeCoul(const vec3 &position, float angle, const std::string &vertexShader, const std::string &fragmentShader,
             float taille, int col_r255, int col_g255, int col_b255, bool degradeCouleur = true);

protected:

    // On redéfinit la méthode de dessin, qui n'est plus compatible avec celle de la classe mère
    void envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) override;


};


#endif //VIDEO_GAME_M2FES_CUBECOUL_H
