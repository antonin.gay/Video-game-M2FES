//
// Created by Antonin on 03/05/2018.
//

#include "Carre.h"

Carre::Carre() : Objet3D(), taille(0) {}

// On reprend le constructeur complet d'Objet3D, en laissant les tailles de tableaux de couleur et textures à définir
Carre::Carre(const vec3 &position, float angle, const std::string &vertexShader,
             const std::string &fragmentShader, float taille, int tailleTabCoul, int tailleTabTexture) :
        Objet3D(6, position, angle, vertexShader, fragmentShader, tailleTabCoul, tailleTabTexture), taille(taille)
{

    // Division du paramètre de taille
    taille /= 2;

    // Vertices temporaires
    float verticesTmp[] =
            {taille, 0.0, taille, taille, 0.0, -taille, -taille, 0.0, taille,
             -taille, 0.0, -taille, -taille, 0.0, taille, taille, 0.0, -taille};

    // Recopie des vertices
    for (int i(0); i < 18; i++) {
        vertices[i] = verticesTmp[i];
    }

}

bool Carre::checkCollision(const vec3 &position, const float &rayon) const {
    // TODO : implémenter collision carré
    return false;
}

bool Carre::checkCollision(const vec3 &position, const float &taille, const float &angle) const {
    // TODO : implémetner collision carré
    return false;
}

bool
Carre::checkCollision(const vec3 &position, const float &tailleX, const float &tailleY, const float &tailleZ) const {
    return false;
}
