//
// Created by Antonin on 03/05/2018.
//

/*
 * Une classe fille de Objet3D définissant un carré, qui ensuite est hérité pour devenir coloré ou texturé
 */

#ifndef VIDEO_GAME_M2FES_CARRE_H
#define VIDEO_GAME_M2FES_CARRE_H


#include "Objet3D.h"

class Carre : public Objet3D {

public:

    Carre();

    Carre(const vec3 &position, float angle, const std::string &vertexShader,
          const std::string &fragmentShader, float taille, int tailleTabCoul, int tailleTabTexture);

    bool checkCollision(const vec3 &position, const float &rayon) const override;

    bool checkCollision(const vec3 &position, const float &taille, const float &angle) const override;

    bool checkCollision(const vec3 &position, const float &tailleX, const float &tailleY,
                        const float &tailleZ) const override;

protected:
    float taille;
};


#endif //VIDEO_GAME_M2FES_CARRE_H
