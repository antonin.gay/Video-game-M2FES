//
// Created by Antonin on 04/05/2018.
//

#include "CubeCoul.h"


CubeCoul::CubeCoul() : Cube() {}

CubeCoul::CubeCoul(const vec3 &position, float angle, const std::string &vertexShader,
                   const std::string &fragmentShader, float taille, int col_r255, int col_g255, int col_b255,
                   bool degradeCouleur)
        : Cube(position, angle, vertexShader, fragmentShader, taille, 108, 0) {

    // On transforme les couleurs en float
    auto col_r = static_cast<float>(col_r255 / 255.0);
    auto col_g = static_cast<float>(col_g255 / 255.0);
    auto col_b = static_cast<float>(col_b255 / 255.0);

    // On crée un tableau temporaire des couleurs
    float couleursTmp[] =
            {col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 1
             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 1

             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 2
             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 2

             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 3
             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 3

             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 4
             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 4

             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 5
             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 5

             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 6
             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b};          // Face 6


    // On copie les données, en faisant varier légèrement les couleurs des faces les unes par rapport aux autres.
    // En effet, tant qu'on n'a pas de lumière et d'ombre, cette légère différence de couleur permet de différencier les
    // faces
    for (int i(0); i < 108; i++)
        couleurs[i] = static_cast<float>(couleursTmp[i] * (1.0 - degradeCouleur * (0.05 * floor(i / 18))));
}

void CubeCoul::envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview)
{
    // Envoi des matrices
    shader.envoyerMat4("modelviewProjection", projection * modelview);

    // Rendu
    glDrawArrays(GL_TRIANGLES, 0, nbVertices);

}
