//
// Created by Antonin on 12/05/2018.
//

#include "CarreCoul.h"

CarreCoul::CarreCoul() : Carre() {}

CarreCoul::CarreCoul(const vec3 &position, float angle, const std::string &vertexShader,
                     const std::string &fragmentShader, float taille, int col_r255, int col_g255, int col_b255)
        : Carre(position, angle, vertexShader, fragmentShader, taille, 18, 0)
{
    // On transforme les couleurs en float
    auto col_r = static_cast<float>(col_r255 / 255.0);
    auto col_g = static_cast<float>(col_g255 / 255.0);
    auto col_b = static_cast<float>(col_b255 / 255.0);

    // On crée un tableau temporaire des couleurs
    float couleursTmp[] =
            {col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 1
             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b};           // Face 1

    // On copie les données.
    for (int i(0); i < 18; i++)
        couleurs[i] = couleursTmp[i];

}

void CarreCoul::envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview)
{
    // Envoi des matrices
    shader.envoyerMat4("modelviewProjection", projection * modelview);

    // Rendu
    glDrawArrays(GL_TRIANGLES, 0, nbVertices);
}
