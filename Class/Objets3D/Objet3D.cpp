//
// Created by Antonin on 27/04/2018.
//

#include "Objet3D.h"

Objet3D::Objet3D() :  nbVertices(0), position(vec3(0,0,0)), angle(0),
                      vaoId(0), vboId(0), tailleVerticesBytes(0), shader(), inverserCollisions(false)
{}


Objet3D::Objet3D(int nbVertices, vec3 position, float angle, std::string const vertexShader,
                 std::string const fragmentShader, int tailleTabCoul, int tailleTabTexture) :
        nbVertices(nbVertices), position(position), angle(angle), vaoId(0), vboId(0), tailleVerticesBytes(nbVertices * sizeof(float)),
        shader(Shader(vertexShader, fragmentShader)), inverserCollisions(false)
{
    // On définit les tailles en bytes des données à envoyer dans la mémoire graphique
    tailleVerticesBytes = 3 * nbVertices * sizeof(float);
    tailleCoulBytes = tailleTabCoul * sizeof(float);
    tailleCoordTextBytes = tailleTabTexture * sizeof(float);

    // La création des tableaux, de vertices, couleurs et textures.
    // Leurs tailles dépendent de l'objet en question
    vertices = new float[3 * nbVertices];
    couleurs = new float[tailleTabCoul];
    coordTexture = new float[tailleTabTexture];

    // Chargement du shader
    shader.charger();
}


Objet3D::~Objet3D() {
    // Destruction du VBO
    glDeleteBuffers(1, &vboId);

    // Destruction du VAO
    glDeleteVertexArrays(1, &vaoId);

    // Destruction des tableaux
    delete vertices;
    delete couleurs;
    delete coordTexture;
}


void Objet3D::charger() {
    /*
     * Cette fonction charge dans la carte graphique le VBO et le VAO.
     * Pour cela, elle fait appel aux deux fonctions correspondantes, définies dans les classes filles.
     */
    chargerVBO();
    chargerVAO();
}

void Objet3D::afficher(glm::mat4 &projection, glm::mat4 &modelview) {
    /*
     * Cette fonction fait appel aux autres méthodes pour verrouiller et déverrouiller le shader et VAO
     * Puis envoi les données au shader, à partir de la fonction recodée dans les classes filles
     * Et enfin, affiche l'objet à l'aide des méthodes des classes filles
     */

    verrouillerShaderVao();

    // On déplace la matrice modelview pour être aux bonnes coordonnées
    glm::mat4 sauvegardeModelView = modelview;
    modelview = glm::translate(modelview, position);
    modelview = glm::rotate(modelview, angle, glm::vec3(0, 1, 0));

    // On envoi le shader et les array pour l'affichage
    envoiShaderEtDessinArray(projection, modelview);

    // Restauration de la modelview originale
    modelview = sauvegardeModelView;

    deverrouillerShaderVao();
}

void Objet3D::verrouillerShaderVao() {
    /*
     * Cette fonction sert à verrouiller shader et VAO, avant d'envoyer les données au shader et
     * d'afficher l'objet
     */

    // Activation du shader
    glUseProgram(shader.getProgramID());

    // Verrouillage VAO
    glBindVertexArray(vaoId);
}

void Objet3D::deverrouillerShaderVao() {
    /*
     * Cette fonction sert à déverrouiller shader et VAO, après avoir affiché l'objet
     */

    // Déverrouillage VAO
    glBindVertexArray(0);

    // Désactivation du shader
    glUseProgram(0);

}

const vec3 Objet3D::getPosition() const {
    return position;
}

float Objet3D::getAngle() const {
    return angle;
}

void Objet3D::setAngle(float angle) {
    Objet3D::angle = angle;
    while (angle > 2 * PI) angle -=2 * PI;
    while (angle < 0)  angle += 2 * PI;
}

void Objet3D::deplacer(vec3 deplacement)
{
    // Cette méthode déplace l'objet selon le vecteur donné
    position += deplacement;
}

void Objet3D::tourner(float rotation) {
    // Cette méthode ajoute l'angle donné
    angle += rotation;
    while (angle > 2* PI) angle -= 2 * PI;
    while (angle < 0)  angle += 2 * PI;
}

void Objet3D::setPosition(vec3 position) {
    // Cette méthode permet d'imposer une position
    this->position = position;
}

void Objet3D::chargerVBO()
{
    /*
     * Cette méthode charge le VBO. Elle est commune aux objets avec ou sans textures ou couleurs.
     * Normalement, elle devrait même fonctionner avec un objet à moitié en couleur, à moitié en textures.
     * Mais cela n'a pas été vérifié.
     */

    // On vérifie que le VBO n'existe pas déjà, sinon on le détruit
    if(glIsBuffer(vboId) == GL_TRUE)
        glDeleteBuffers(1, &vboId);

    // Génération de l'Id
    glGenBuffers(1, &vboId);

    // Verrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, vboId);

    // Allocation de la mémoire
    glBufferData(GL_ARRAY_BUFFER, tailleVerticesBytes + tailleCoordTextBytes + tailleCoulBytes, nullptr,
                 GL_STATIC_DRAW);

    // Envoi des données vers la mémoire en fonction du type d'objet : couleur ou texture
    if(tailleCoordTextBytes != 0)
    {
        // Transfert des données textures
        glBufferSubData(GL_ARRAY_BUFFER, 0, tailleVerticesBytes, vertices);
        glBufferSubData(GL_ARRAY_BUFFER, tailleVerticesBytes, tailleCoordTextBytes, coordTexture);
    }
    else if (tailleCoulBytes != 0)
    {
        // Transfert des données couleurs
        glBufferSubData(GL_ARRAY_BUFFER, 0, tailleVerticesBytes, vertices);
        glBufferSubData(GL_ARRAY_BUFFER, tailleVerticesBytes + tailleCoordTextBytes, tailleCoulBytes, couleurs);
    }

    // Déverrouillage de l'objet
    glBindBuffer(GL_ARRAY_BUFFER, 0);

}

void Objet3D::chargerVAO()
{
    /*
     * Cette méthode charge le VAO. Elle est commune aux objets avec ou sans textures ou couleurs.
     * Normalement, elle devrait même fonctionner avec un objet à moitié en couleur, à moitié en textures.
     * Mais cela n'a pas été vérifié.
     */

    // On vérifie que le VAO n'existe pas déjà, sinon on le détruit
    if(glIsVertexArray(vaoId) == GL_TRUE) {
        (1, &vaoId);
    }

    // Génération de l'Id
    glGenVertexArrays(1, &vaoId);

    // Verrouillage du VAO
    glBindVertexArray(vaoId);

    // Verrouillage VBO
    glBindBuffer(GL_ARRAY_BUFFER, vboId);

    // Envoi des vertices
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(0);

    if (tailleCoordTextBytes != 0)
    {
        // Envoi des coordonnées de texture
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(tailleVerticesBytes));
        glEnableVertexAttribArray(2);
    }
    else if (tailleCoulBytes != 0)
    {
        // On rentre les couleurs dans le tableau Vertex Attrib 1
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(tailleVerticesBytes + tailleCoordTextBytes));
        glEnableVertexAttribArray(1);
    }

    // Déverrouillage VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Déverrouillage VAO
    glBindVertexArray(0);

}

void Objet3D::setInverserCollisions(bool inverserCollisions) {
    Objet3D::inverserCollisions = inverserCollisions;
}
