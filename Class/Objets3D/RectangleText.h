//
// Created by mbauw on 5/14/18.
//

/*
 * Un rectangle avec une texture, qu'on peut recopier de nombreuses fois sur l'objet
 *
 * - la classe Texture est-elle adaptée ici au même titre que pour Carre ?!
 */

#ifndef VIDEO_GAME_M2FES_RECTANGLETEXT_H
#define VIDEO_GAME_M2FES_RECTANGLETEXT_H

#include "Rectangle.h"
#include "Texture.h"

class RectangleText : public Rectangle {

public:
    // le constructeur classique: la texture prend tout l'objet
    RectangleText(vec3 position, float angle, const std::string &vertexShader, const std::string &fragmentShader,
    float longueur, float largeur, const std::string &texturePath);

    // le constructeur de texture pavée: on dit en paramètre le nombre de fois que la texture est répétée
    RectangleText(vec3 position, float angle, const std::string &vertexShader, const std::string &fragmentShader,
    float longueur, float largeur, const std::string &texturePath, int nbRepetitionText);

private:

    void envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) override;

protected:
    Texture texture;

};


#endif //VIDEO_GAME_M2FES_RECTANGLETEXT_H
