//
// Created by mbauw on 5/13/18.
//

/*
 * Une classe fille de Objet3DD définissant un carré, qui ensuite est hérité pour devenir coloré ou texturé
 *
 * Par rapport à Carre on met deux paramètres de taille: longeur et largeur, sachant qu'un des deux paramètres vaudra
 * pour dimensions de l'espace
 *
 * Comme pour Carre, position est un reference
 */

#ifndef VIDEO_GAME_M2FES_RECTANGLE_H
#define VIDEO_GAME_M2FES_RECTANGLE_H

#include "Objet3D.h"

class Rectangle : public Objet3D {

public:

    Rectangle();

    Rectangle(const vec3 &position, float angle, const std::string &vertexShader,
              const std::string &fragmentShader, float longueur, float largeur, int tailleTabCoul,
              int tailleTabTexture);

    bool checkCollision(const vec3 &position, const float &rayon) const override;

    bool checkCollision(const vec3 &position, const float &taille, const float &angle) const override;

protected:
    float longueur;
    float largeur;
};


#endif //VIDEO_GAME_M2FES_RECTANGLE_H
