//
// Created by Antonin on 02/05/2018.
//

#include "CubeText.h"

CubeText::CubeText(vec3 position, float angle, const std::string &vertexShader,
                   const std::string &fragmentShader, float taille, const std::string &texturePath) :
        Cube(position, angle, vertexShader, fragmentShader, taille, 0, 72)
{
    // On crée et charge la texture
    texture = Texture(texturePath);
    texture.charger();

    // Coordonnées de texture temporaires
    float coordTextureTmp[] = {0, 0, 1, 0, 1, 1,     // Face 1
                               0, 0, 0, 1, 1, 1,     // Face 1

                               0, 0, 1, 0, 1, 1,     // Face 2
                               0, 0, 0, 1, 1, 1,     // Face 2

                               0, 0, 1, 0, 1, 1,     // Face 3
                               0, 0, 0, 1, 1, 1,     // Face 3

                               0, 0, 1, 0, 1, 1,     // Face 4
                               0, 0, 0, 1, 1, 1,     // Face 4

                               0, 0, 1, 0, 1, 1,     // Face 5
                               0, 0, 0, 1, 1, 1,     // Face 5

                               0, 0, 1, 0, 1, 1,     // Face 6
                               0, 0, 0, 1, 1, 1};    // Face 6

    // On recopie les données
    for (int i(0); i < 72; i++)
        coordTexture[i] = coordTextureTmp[i];

}


void CubeText::envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) {
    // Verrouillage de la texture
    glBindTexture(GL_TEXTURE_2D, texture.getID());

    // Envoi des matrices
    shader.envoyerMat4("modelviewProjection", projection * modelview);

    // Rendu
    glDrawArrays(GL_TRIANGLES, 0, nbVertices);

    // Déverrouillage de la texture
    glBindTexture(GL_TEXTURE_2D, 0);

}
