//
// Created by mbauw on 5/14/18.
//

#ifndef VIDEO_GAME_M2FES_RECTANGLECOUL_H
#define VIDEO_GAME_M2FES_RECTANGLECOUL_H

#include "Rectangle.h"

class RectangleCoul : public Rectangle {

public:
    RectangleCoul();

    RectangleCoul(const vec3 &position, float angle, const std::string &vertexShader,
    const std::string &fragmentShader, float longueur, float largeur, int col_r255, int col_g255, int col_b255);

protected:



    // on redéfinit la méthode de dessin, qui n'était plus compatible
    void envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) override;
};


#endif //VIDEO_GAME_M2FES_RECTANGLECOUL_H
