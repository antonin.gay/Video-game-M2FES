//
// Created by Antonin on 02/05/2018.
//

#include "Cube.h"

Cube::Cube() : Objet3D(), taille(0)
{}

/*
 * Constructeur de la classe. On fait appel au constructeur complet de l'Objet3D, en lui donnant comme paramètre
 * 36 vertices. Le constructeur prend en argument la taille des tableaux de textures et couleurs, pour plus tard
 */
Cube::Cube(vec3 position, float angle, const std::string &vertexShader,
           const std::string &fragmentShader, float taille, int tailleTabCoul, int tailleTabTexture) :
                Objet3D(36, position, angle, vertexShader,
                fragmentShader, tailleTabCoul, tailleTabTexture), taille(taille)
{

    // Division du paramètre taille
    taille /= 2;

    // Vertices temporaires
    float verticesTmp[] =
            {-taille, -taille, -taille,   taille, -taille, -taille,   taille, taille, -taille,     // Face 1
             -taille, -taille, -taille,   -taille, taille, -taille,   taille, taille, -taille,     // Face 1

             taille, -taille, taille,   taille, -taille, -taille,   taille, taille, -taille,       // Face 2
             taille, -taille, taille,   taille, taille, taille,   taille, taille, -taille,         // Face 2

             -taille, -taille, taille,   taille, -taille, taille,   taille, -taille, -taille,      // Face 3
             -taille, -taille, taille,   -taille, -taille, -taille,   taille, -taille, -taille,    // Face 3

             -taille, -taille, taille,   taille, -taille, taille,   taille, taille, taille,        // Face 4
             -taille, -taille, taille,   -taille, taille, taille,   taille, taille, taille,        // Face 4

             -taille, -taille, -taille,   -taille, -taille, taille,   -taille, taille, taille,     // Face 5
             -taille, -taille, -taille,   -taille, taille, -taille,   -taille, taille, taille,     // Face 5

             -taille, taille, taille,   taille, taille, taille,   taille, taille, -taille,         // Face 6
             -taille, taille, taille,   -taille, taille, -taille,   taille, taille, -taille};      // Face 6


    // Recopie des vertices
    for (int i(0); i < 108; i++) {
        vertices[i] = verticesTmp[i];
    }

}

bool Cube::checkCollision(const vec3 &positionOther, const float &rayon) const {
    // TODO : amérliorer collisions cube / sphère

    // On utilise pour l'instant la collision entre deux cubes
    return checkCollision(positionOther, rayon, rayon, rayon);
}

bool Cube::checkCollision(const vec3 &positionOther, const float &tailleOther, const float &angle) const {

    // TODO : améliorer collision cube / cube
    // On vérifie la collision entre le cube et un cercle de positionOther et rayon donné.

    // Première implémentation : on se base sur celle de la sphère en approximant notre cube à une sphère de rayon la demi-
    // longueur du cube

    return this->checkCollision(positionOther, tailleOther / 2);
}

bool
Cube::checkCollision(const vec3 &positionOther, const float &tailleX, const float &tailleY, const float &tailleZ) const {
    // On vérifie la collision entre le cube et un cercle de positionOther et rayon donné.

    float angleRad = angle * 2.0f * PI / 360.0f;

    float positionRelativeX = positionOther.x - position.x;
    float positionRelativeY = positionOther.y - position.y;
    float positionRelativeZ = positionOther.z - position.z;

    // On projète les coordonnées de l'autre dans notre référentiel
    float newX = positionRelativeX * cos(angleRad) + positionRelativeZ * sin(angleRad);
    float newY = positionRelativeY;
    float newZ = positionRelativeX * -1.0f * sin(angleRad) + positionRelativeZ * cos(angleRad);
    vec3 positionOtherProj = vec3(newX, newY, newZ);

    // Première implémentation : on ne fait que vérifier que les deux centres sont espacés de plus du
    // rayon + longueur max du cube, et qu'ils sont sur la même hauteur
    bool collisionX = abs(positionOtherProj.x) <= tailleX + taille / 2.0;
    bool collisionY = abs(positionOtherProj.y) <= tailleY + taille / 2.0;
    bool collisionZ = abs(positionOtherProj.z) <= tailleZ + taille / 2.0;

    bool collision = collisionX && collisionY && collisionZ;

    return !inverserCollisions == collision;
}
