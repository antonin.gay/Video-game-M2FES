//
// Created by mbauw on 5/14/18.
//

#include "RectangleCoul.h"

RectangleCoul::RectangleCoul() : Rectangle() {}

RectangleCoul::RectangleCoul(const vec3 &position, float angle, const std::string &vertexShader,
                             const std::string &fragmentShader, float longueur, float largeur, int col_r255,
                             int col_g255, int col_b255)
            : Rectangle(position, angle, vertexShader, fragmentShader, longueur, largeur, 18, 0)
{
    // on transforme les couleurs en float
    auto col_r = static_cast<float>(col_r255 / 255.0);
    auto col_g = static_cast<float>(col_g255 / 255.0);
    auto col_b = static_cast<float>(col_b255 / 255.0);

    // On crée un tableau temporaire des couleurs
    float couleursTmp[] =
            {col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b,           // Face 1
             col_r, col_g, col_b, col_r, col_g, col_b, col_r, col_g, col_b};           // Face 1

    // On copie les données.
    for (int i(0); i < 18; i++)
        couleurs[i] = couleursTmp[i];
}

void RectangleCoul::envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview)
{
    // envoi des matrices
    shader.envoyerMat4("modelviewProjection", projection * modelview);

    // rendu
    glDrawArrays(GL_TRIANGLES, 0, nbVertices);
}