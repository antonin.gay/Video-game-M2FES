//
// Created by mbauw on 5/13/18.
//

#include "Rectangle.h"

Rectangle::Rectangle() : Objet3D(), longueur(0), largeur(0) {}

/* - reprise du constructeur complet d'Objet3D, en laissant les tailles de tableaux de couleur et textures à définir
 * - même nombre de vertices que pour Carre.cpp
 * - &position pour Carre, mais position pour Objet3D
*/

Rectangle::Rectangle(const vec3 &position, float angle, const std::string &vertexShader,
                     const std::string &fragmentShader, float longueur, float largeur, int tailleTabCoul,
                     int tailleTabTexture) : Objet3D(6, position, angle, vertexShader, fragmentShader, tailleTabCoul,
                     tailleTabTexture), longueur(longueur), largeur(largeur)
{

    // Division des paramètres de taille
    longueur /= 2;
    largeur /= 2;

    // Vertices temporaires
    float verticesTmp[] =
            {longueur, 0.0, largeur, longueur, 0.0, -largeur, -longueur, 0.0, largeur,
             -longueur, 0.0, -largeur, -longueur, 0.0, largeur, longueur, 0.0, -largeur};

    for (int i(0); i < 18; i++) {
        vertices[i] = verticesTmp[i];
    }
}

bool Rectangle::checkCollision(const vec3 &position, const float &rayon) const {
    return false;
}

bool Rectangle::checkCollision(const vec3 &position, const float &taille, const float &angle) const {
    return false;
}
