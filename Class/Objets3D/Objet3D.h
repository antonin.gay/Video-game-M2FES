//
// Created by Antonin on 27/04/2018.
//

#ifndef VIDEO_GAME_M2FES_OBJET3D_H
#define VIDEO_GAME_M2FES_OBJET3D_H

// Includes OpenGL

#ifdef WIN32
#include <GL/glew.h>
#else
#define GL3_PROTOTYPES 1
#include <GL3/gl3.h>
#endif

// Includes GLM
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#ifndef BUFFER_OFFSET
#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))
#endif

// Includes
#include "Shader.h"

#define PI 3.14

using namespace glm;

class Objet3D {

public :

    Objet3D();

    // Constructeur de l'objet 3D. Le paramètre position est par rapport au centre de l'objet
    Objet3D(int nbVertices, vec3 position, float angle, std::string vertexShader,
            std::string fragmentShader, int tailleTabCoul, int tailleTabTexture);

    virtual ~Objet3D();

    // Charge le VBO et VAO de l'objet 3D
    void charger();

    // Affiche l'objet
    void afficher(glm::mat4 &projection, glm::mat4 &modelview);

    // Permet de déplacer par rapport à un vecteur
    void deplacer(vec3 deplacement);

    // La rotation est par rapport à l'axe Y (vertical)
    void tourner(float rotation);

    const vec3 getPosition() const;

    float getAngle() const;

    void setAngle(float angle);

    void setPosition(vec3 position);

    void setInverserCollisions(bool inverserCollisions);

    /*
     * Les deux méthodes suivantes sont là pour tester les collisions avec un autre objet.
     * Pour l'instant, seules deux méthodes ont été codées, mais on pourrait en définir d'autres, par exemple de
     * collisions avec un cube ou une de nos autres formes.
     * Elles servent donc à la gestion des collisions, afin de pouvoir réaliser un test directement sur l'objet de si il
     * est, ou non, en collision avec l'autre objet.
     * Elles renvoient un booleen de si elles sont en collision, c'est à dire si "elles partagent un morceau d'espace avec l'autre forme"
     */

    // La première, prévue pour une collision avec une sphère, prend la position du centre de la sphère et son rayon
    virtual bool checkCollision(const vec3 &position, const float &rayon) const = 0;

    // La deuxième est prévue pour rencontrer un autre cube, de position, taille et angle donné
    virtual bool checkCollision(const vec3 &position, const float &taille, const float &angle) const = 0;

    virtual bool
    checkCollision(const vec3 &position, const float &tailleX, const float &tailleY, const float &tailleZ) const = 0;

protected :

    int nbVertices;     // Le nombre de vertices nécessaires à tracer l'objet
    vec3 position;      // La position du centre de l'objet
    float angle;        // Degrés, par autour de l'axe vertical
    GLuint vaoId;       // Id du VAO
    GLuint vboId;

    // Taille, en bytes, sur la mémoire graphique, des différents éléments qui y sont stockés
    int tailleVerticesBytes;
    int tailleCoordTextBytes;
    int tailleCoulBytes;

    // Les pointeurs vers les tableaux de points. Leur taille variant d'un objet à l'autre, elles ne sont pas précisées
    float *vertices;
    float *coordTexture;
    float *couleurs;

    Shader shader;      // Le shader pour l'affichage de l'objet 3D

    bool inverserCollisions;
        // Si vaut true, on inverse le calcul de collision (exemple : on empêche de sortir d'un cube)

    // Les fonctions qui verrouillent et déverrouillent Shader, VAO et VBO avant et après affichage
    void verrouillerShaderVao();
    void deverrouillerShaderVao();

    // Méthodes pour charger le VBO et VAO, lancées une seule fois, au moment de l'initialisation de l'objet
    void chargerVBO();
    void chargerVAO();

    // Méthode chargée d'appeler la fonction 'DrawArray' avec les bons paramètres
    void dessinerArray() {
        glDrawArrays(GL_TRIANGLES, 0, nbVertices/3);
    }

    // Gestion du shader et du dessin. Intègre les trois fonctions précédentes. Elle peut être override dans une classe
    // fille si les fonctions précédentes ne conviennent pas
    virtual void envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) = 0;

};

#endif //VIDEO_GAME_M2FES_OBJET3D_H
