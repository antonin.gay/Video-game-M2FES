//
// Created by Antonin on 12/05/2018.
//

#ifndef VIDEO_GAME_M2FES_CARRECOUL_H
#define VIDEO_GAME_M2FES_CARRECOUL_H


#include "Carre.h"

class CarreCoul : public Carre {

public:
    CarreCoul();

    CarreCoul(const vec3 &position, float angle, const std::string &vertexShader, const std::string &fragmentShader,
              float taille, int col_r255, int col_g255, int col_b255);

protected:

    // On redéfinit la méthode de dessin, qui n'était plus compatible
    void envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) override;
};


#endif //VIDEO_GAME_M2FES_CARRECOUL_H
