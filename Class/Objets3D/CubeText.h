//
// Created by Antonin on 02/05/2018.
//

/*
 * Un Cube avec ses faces texturées, toutes avec la même texture.
 */

#ifndef VIDEO_GAME_M2FES_CUBETEXT_H
#define VIDEO_GAME_M2FES_CUBETEXT_H

#include "Cube.h"
#include "Texture.h"

using namespace glm;

class CubeText : public Cube
{
public:
    CubeText(vec3 position, float angle, const std::string &vertexShader, const std::string &fragmentShader,
             float taille, const std::string &texturePath);


private:
    Texture texture;

protected:

    void envoiShaderEtDessinArray(glm::mat4 &projection, glm::mat4 &modelview) override;

};


#endif //VIDEO_GAME_M2FES_CUBETEXT_H
