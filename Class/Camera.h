//
// Created by Antonin on 27/03/2018.
//

#ifndef OPCL_CLION_CAMERA_H
#define OPCL_CLION_CAMERA_H

// Include

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Input.h"

using namespace glm;

class Camera
{
public:
    Camera();

    Camera(vec3 position, vec3 pointCible, vec3 axeVertical, float sensibilite, float vitesse);

    ~Camera();

    void orienter(int xRel, int yRel);

    void deplacer(Input const &input);

    void lookAt(mat4 &modelview);

    void setPointCible(const vec3 &m_pointCible);

    void setPosition(const vec3 &m_position);

    void setSensibilite(float m_sensibilite);

    void setVitesse(float m_vitesse);

    float getSensibilite() const;

    float getVitesse() const;

    const vec3 &getOrientation() const;

private:
    float phi;
    float m_theta;
    vec3 orientation;

    vec3 axeVertical;
    vec3 deplacementLateral;

    vec3 position;
    vec3 pointCible;

    float m_sensibilite;
    float m_vitesse;

    void updateOrientation();
};


#endif //OPCL_CLION_CAMERA_H
