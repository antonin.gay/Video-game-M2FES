//
// Created by Antonin on 02/05/2018.
//

#include "Personnage.h"

Personnage::Personnage() :
        taille(0), position(0,0,0), orientation(0,0,0),
        corps(new CubeCoul()), tete(new CubeCoul()), pieds(new CubeCoul()) {}

Personnage::Personnage(float taille, vec3 position, vec3 orientation) :
    taille(taille), position(position), orientation(orientation)
{
    // On calcule l'orientation vers la droite
    updateOrientationDroite();

    // On initialise les trois cubes formant notre personnage.
    float tailleCube = taille / 3;
    float angle = getAngleOrientation();

    // La tete est située 2*taille/3 au-dessus de la référence de position
    tete = new CubeCoul(this->position + vec3(0, tailleCube, 0), angle,
                        "../Shaders/couleur3D.vert", "../Shaders/couleur3D.frag", tailleCube,
                        15 * 16 + 12, 14 * 16 + 7, 13 * 16 + 9);
    tete->charger();


    // Le corps est situé taille/3 au-dessus de la référence de position
    corps = new CubeCoul(this->position, angle,
                         "../Shaders/couleur3D.vert", "../Shaders/couleur3D.frag", tailleCube,
                         15 * 16 + 4, 4 * 16 + 3, 3 * 16 + 6);
    corps->charger();


    // Les pieds sont au niveau du sol
    pieds = new CubeCoul(this->position - vec3(0, tailleCube, 0), angle,
                         "../Shaders/couleur3D.vert", "../Shaders/couleur3D.frag", tailleCube,
                         3 * 16 + 9, 4 * 16 + 9, 10 * 16 + 11);
    pieds->charger();
}

void Personnage::deplacer(vec3 deplacement)
{
    // Lors d'un déplacement, la position est incrémentée
    position += deplacement;

    // Les trois objets sont déplacés dans la bonne direction
    corps->deplacer(deplacement);
    pieds->deplacer(deplacement);
    tete->deplacer(deplacement);

    // Le corps et les pieds sont alignés à la position de déplacement

    float angle = getAngleOrientation();
    corps->setAngle(angle);
    pieds->setAngle(angle);
}

void Personnage::tourner(vec3 rotation)
{
    // En cas de rotation, seule la tête tourne, le corps et les pieds restant alignés au déplacemement.
    orientation += rotation;
    orientation = normalize(orientation);
    updateOrientationDroite();

    tournerTete();
}

void Personnage::afficher(mat4 &projection, mat4 &modelview)
{
    // On affiche les trois objets 3D
    tete->afficher(projection, modelview);
    corps->afficher(projection, modelview);
    pieds->afficher(projection, modelview);
}

Personnage::~Personnage()
{
    // On détruit les trois pointeurs
    delete pieds;
    delete corps;
    delete tete;
}

void Personnage::tournerTete()
{
    // On orient la tête vers le nouvel angle calculé
    float angleTete = getAngleOrientation();
    tete->setAngle(angleTete);

    // TODO : gérer angle différentiel entre corps et tête
    // Si la tête et le corps sont trop séparés en rotation, on fait tourner le corps avec
    float differenceAngles = angleTete - pieds->getAngle();

    while(differenceAngles > PI)
        differenceAngles -= 2 * PI;
    while (differenceAngles < -PI)
        differenceAngles += 2 * PI;

    if(differenceAngles > PI / 4)
    {
        corps->setAngle(static_cast<float>(angleTete - (PI / 4)));

        if(differenceAngles > PI / 3)
            pieds->setAngle(static_cast<float>(angleTete - (PI / 3)));
    }
    else if (differenceAngles < -PI / 4)
    {
        corps->setAngle(static_cast<float>(angleTete + (PI / 4)));
        if(differenceAngles < -PI/3)
            pieds->setAngle(static_cast<float>(angleTete + (PI / 3)));
    }
}

float Personnage::getAngleOrientation()
{
    vec2 orientation2D = normalize(vec2(orientation.x, orientation.z));
    float angle = acos(orientation2D.x);
    if(asin(orientation2D.y) > 0)
        angle *= -1;

    return angle;
}

const vec3 &Personnage::getOrientation() const {
    return orientation;
}

const vec3 &Personnage::getPosition() const {
    return position;
}

const vec3 &Personnage::getPositionPieds() const {
    return pieds->getPosition();
}

const vec3 &Personnage::getOrientationDroite() const {
    return orientationDroite;
}

void Personnage::setOrientation(const vec3 &orientation) {
    Personnage::orientation = orientation;
    updateOrientationDroite();
    tournerTete();
}

void Personnage::updateOrientationDroite()
{
    // On calcule le vecteur qui pointe vers la droite du personnage à partir de son orientation
    orientationDroite = cross(orientation , vec3(0, 1, 0));
    orientationDroite = normalize(orientationDroite);
}

