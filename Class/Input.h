//
// Created by Antonin on 26/03/2018.
//

#ifndef OPCL_CLION_INPUT_H
#define OPCL_CLION_INPUT_H

// Include

#include <SDL2/SDL.h>


// Classe

class Input
{
public:

    Input();
    ~Input();

    void updateEvenements();

    bool terminer() const;

    bool getTouche(SDL_Scancode touche) const;

    bool getBoutonSouris(Uint8 bouton) const;

    bool mouvementSouris() const;

    int getM_x() const;

    int getM_y() const;

    int getM_xRel() const;

    int getM_yRel() const;

    void afficherPointeur(bool reponse) const;

    void capturerPointeur(bool reponse) const;

private:

    SDL_Event m_evenement;
    bool m_touches[SDL_NUM_SCANCODES];
    bool m_boutonsSouris[8];

    int m_x, m_y, m_xRel, m_yRel;

    bool m_terminer;

};

#endif
