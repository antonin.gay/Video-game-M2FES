//
// Created by Antonin on 02/05/2018.
//

#ifndef VIDEO_GAME_M2FES_PERSONNAGE_H
#define VIDEO_GAME_M2FES_PERSONNAGE_H

#include "Objets3D/CubeCoul.h"
#include <cmath>

using namespace glm;

class Personnage {
public:
    Personnage();

    Personnage(float taille, vec3 position, vec3 orientation);

    virtual ~Personnage();

    // Translate selon un vecteur
    void deplacer(vec3 deplacement);

    // Oriente le personnage
    void tourner(vec3 rotation);

    void afficher(mat4 &projection, mat4 &modelview);

    // Getter & Setter
    const vec3 &getOrientation() const;

    const vec3 &getOrientationDroite() const;

    const vec3 &getPosition() const;

    const vec3 &getPositionPieds() const;

    void setOrientation(const vec3 &orientation);

private:
    float taille;                       // Taille totale des 3 cubes
    vec3 position;                      // Position du centre du personnage
    CubeCoul *pieds, *corps, *tete;     // Un personnage de trois cubes colorés : POINTEURS
    vec3 orientation, orientationDroite;// L'orientation de son regard, et un vecteur orthogonal horizontal pointant vers la droite du personnage

    void updateOrientationDroite();     // Recalcule le vecteur pointant vers la droite

    void tournerTete();                 // Méthode qui oriente la tête dans le sens du regard

    float getAngleOrientation();        // Méthode calculant l'angle par rapport au vecteur (1,0,0) du regard
};


#endif //VIDEO_GAME_M2FES_PERSONNAGE_H
