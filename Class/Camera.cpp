//
// Created by Antonin on 27/03/2018.
//

#include "Camera.h"

Camera::Camera() : phi(0.0), m_theta(0.0), orientation(), axeVertical(0, 1, 0),
                   deplacementLateral(), position(), pointCible(), m_sensibilite(0.0), m_vitesse(0.0)
{

}

Camera::Camera(vec3 position, vec3 pointCible, vec3 axeVertical, float sensibilite, float vitesse) :
        phi(0.0), m_theta(0.0), orientation(),
        axeVertical(axeVertical),
        deplacementLateral(), position(position),
        pointCible(pointCible), m_sensibilite(sensibilite), m_vitesse(vitesse)
{

//    setPointCible(pointCible);
}

void Camera::orienter(int xRel, int yRel)
{
    // Modification des angles
    phi += -yRel * m_sensibilite;
    m_theta += -xRel * m_sensibilite;

    //Limitation de phi
    if (phi > 0.0) {
        phi = 0.0;
    } else if (phi < -89.0) {
        phi = static_cast<float>(-89.0);
    }

    // Calcul des coordonnées sphériques
    updateOrientation();

    // Calcul de la normale
    deplacementLateral = cross(axeVertical, orientation);
    deplacementLateral = normalize(deplacementLateral);

//    pointCible = position + orientation;
}

void Camera::updateOrientation()
{

    auto phiRadian = static_cast<float>(phi * M_PI / 180.0);
    auto thetaRadian = static_cast<float>(m_theta * M_PI / 180.0);

    // Si l'axe vertical est l'axe X
    if(axeVertical.x == 1.0)
    {
        // Calcul des coordonnées sphériques

        orientation.x = sin(phiRadian);
        orientation.y = cos(phiRadian) * cos(thetaRadian);
        orientation.z = cos(phiRadian) * sin(thetaRadian);
    }


    // Si c'est l'axe Y
    else if(axeVertical.y == 1.0)
    {
        // Calcul des coordonnées sphériques

        orientation.x = cos(phiRadian) * sin(thetaRadian);
        orientation.y = sin(phiRadian);
        orientation.z = cos(phiRadian) * cos(thetaRadian);
    }

    // Sinon c'est l'axe Z
    else
    {
        // Calcul des coordonnées sphériques

        orientation.x = cos(phiRadian) * cos(thetaRadian);
        orientation.y = cos(phiRadian) * sin(thetaRadian);
        orientation.z = sin(phiRadian);
    }
}

void Camera::deplacer(Input const &input)
{
    // Avancée de la caméra
    if(input.getTouche(SDL_SCANCODE_UP))
    {
        position = position + orientation * m_vitesse;
        pointCible = position + orientation;
    }

    // Recul de la caméra
    if(input.getTouche(SDL_SCANCODE_DOWN))
    {
        position = position - orientation * m_vitesse;
        pointCible = position + orientation;
    }

    // Déplacement vers la gauche
    if(input.getTouche(SDL_SCANCODE_LEFT))
    {
        position = position + deplacementLateral * m_vitesse;
        pointCible = position + orientation;
    }

    // Déplacement vers la droite
    if(input.getTouche(SDL_SCANCODE_RIGHT))
    {
        position = position - deplacementLateral * m_vitesse;
        pointCible = position + orientation;
    }

    // Gestion de l'orientation
    if(input.mouvementSouris())
        orienter(input.getM_xRel(), input.getM_yRel());
}

void Camera::setPosition(const vec3 &position)
{
    //Mise à jour de la position
    this->position = position;

//    // Actualisation du point cible
//    pointCible = pointCible + orientation;
}

void Camera::setPointCible(const vec3 &pointCible)
{
    this->pointCible = pointCible;
/*
    // Calcul du vecteur orientation
    orientation = m_pointCible - position;
    orientation = normalize(orientation);

    // Si l'axe vertical est l'axe X
    if(axeVertical.x == 1.0)
    {
        // Calcul des angles
        phi = asin(orientation.x);
        m_theta = acos(orientation.y / cos(phi));

        if(orientation.y < 0)
            m_theta *= -1;
    }

    // Si c'est l'axe Y
    else if (axeVertical.y == 1.0) {
        // Calcul des angles
        phi = asin(orientation.y);
        m_theta = acos(orientation.z / cos(phi));

        if (orientation.z < 0)
            m_theta *= -1;
    }

    // Sinon c'est l'axe Z
    else {

        // Calcul des angles
        phi = asin(orientation.x);
        m_theta = acos(orientation.z / cos(phi));

        if (orientation.z < 0)
            m_theta *= -1;
    }

    // Conversion en degrés
    phi = static_cast<float>(phi * 180 / M_PI);
    m_theta = static_cast<float>(m_theta * 180 / M_PI);
*/
}

void Camera::lookAt(mat4 &modelview)
{
    modelview = glm::lookAt(position, pointCible, axeVertical);
}

float Camera::getSensibilite() const {
    return m_sensibilite;
}

float Camera::getVitesse() const {
    return m_vitesse;
}

void Camera::setSensibilite(float m_sensibilite) {
    Camera::m_sensibilite = m_sensibilite;
}

void Camera::setVitesse(float m_vitesse) {
    Camera::m_vitesse = m_vitesse;
}

const vec3 &Camera::getOrientation() const {
    return orientation;
}

Camera::~Camera() = default;
