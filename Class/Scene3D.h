//
// Created by Antonin on 02/05/2018.
//

#ifndef VIDEO_GAME_M2FES_SCENE3D_H
#define VIDEO_GAME_M2FES_SCENE3D_H

#include <vector>
#include <SDL2/SDL_image.h>

#include "Camera.h"
#include "Input.h"
#include "Objets3D/CubeText.h"
#include "Objets3D/CubeCoul.h"
#include "Objets3D/CarreText.h"
#include "Objets3D/Objet3D.h"
#include "Personnage.h"

using namespace glm;

class Scene3D {

public:
    Scene3D(std::string titreFenetre, int largeurFenetre, int hauteurFenetre);

    virtual ~Scene3D();

    void afficherObjets(mat4 &projection, mat4 &modelview);

    // Cette méthode gère les inputs liés au déplacement et le déplacement
    void deplacerPersonnage();

    void orienterPersonnage();

    void deplacerCamera();

    bool initFenetre();

    bool initOpenGL();

    void bouclePrincipale();

    void ajouterObjet3D(Objet3D *objet3D);

    void setPersonnage(Personnage *personnage);

    bool chargerObjetsImagePNG(const char *filePath);

private:

    Camera camera;
    Input input;
    Personnage *personnage;
    std::vector<Objet3D*> vectorObjets3D;

    std::string titreFenetre;
    int largeurFenetre;
    int hauteurFenetre;

    SDL_Window* fenetre;
    SDL_GLContext contexteOpenGL;

    bool gestionCollision(vec3 &deplacement, Objet3D const *objetCollision, float const rayonCollision);

};


#endif //VIDEO_GAME_M2FES_SCENE3D_H
