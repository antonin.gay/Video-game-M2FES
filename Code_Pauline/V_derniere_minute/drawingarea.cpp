#include "drawingarea.h"
#include <QPainter>

// Constructeur par défaut
DrawingArea::DrawingArea(QWidget *parent)
    : QWidget(parent)
{
    this->defaultWidth=width();
    this->defaultHeight=height();
}

// Réécriture de la méthode paintEvent pour éviter les erreurs d'affichage
void DrawingArea::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.save();
    QPolygon facetPolygon;

    float zScaleFactor=1;
    //float zScaleFactor=1/(this->facets[this->nFacets-1].getBarycentre().getZ()/10);

    for(int i=0;i<this->nFacets;i++) {
        painter.setBrush(facets[i].getColor()); // dessin dans la couleur de la facette
        // Récupération du polygone représentant la facette après application de facteurs d'échelle
        facetPolygon=this->facets[i].getPoints(zScaleFactor*(float)width()/(this->defaultWidth),zScaleFactor*(float)height()/(this->defaultHeight));
        painter.drawPolygon(facetPolygon);
    }
    painter.restore();

    // Zone de dessin
    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.setPen(palette().dark().color());
    painter.setBrush(Qt::NoBrush);
    painter.drawRect(QRect(0, 0, width() - 1, height() - 1));
}

// Récupération du tableau & du nombre de facettes à dessiner
void DrawingArea::setFacetsToDraw(Facette *facets,int nFacets) {
    this->facets=facets;
    this->nFacets=nFacets;
}

int DrawingArea::getHeight(){
    return this->height();
}

int DrawingArea::getWidth(){
    return this->width();
}
