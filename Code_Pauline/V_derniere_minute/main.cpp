#include <QApplication>
#include <QObject>

#include "threadbouton.h"
#include "threaddisplaytimer.h"
#include "window.h"

int main (int argc, char *argv []){
    QApplication app(argc,argv);

    // Création du thread gérant l'affichage
    ThreadDisplayTimer threadDisplayTimer;

    // Création du thread gérant les timers
    ThreadBouton threadBouton(threadDisplayTimer.w,threadDisplayTimer.pave);

    // Connexion des signaux
    QObject::connect(threadDisplayTimer.w->xSlider,SIGNAL(valueChanged(int)),&threadBouton,SLOT(translationxSL(int)));
    QObject::connect(&threadBouton,SIGNAL(translationx(Point3d)),threadDisplayTimer.pave,SLOT(translation(Point3d)));
    QObject::connect(threadDisplayTimer.w->ySlider,SIGNAL(valueChanged(int)),&threadBouton,SLOT(translationySL(int)));
    QObject::connect(&threadBouton,SIGNAL(translationy(Point3d)),threadDisplayTimer.pave,SLOT(translation(Point3d)));
    QObject::connect(threadDisplayTimer.w->zSlider,SIGNAL(valueChanged(int)),&threadBouton,SLOT(translationzSL(int)));
    QObject::connect(&threadBouton,SIGNAL(translationz(Point3d)),threadDisplayTimer.pave,SLOT(translation(Point3d)));

    QObject::connect(threadDisplayTimer.w->rxSlider,SIGNAL(valueChanged(int)),&threadBouton,SLOT(rotationxSL(int)));
    QObject::connect(&threadBouton,SIGNAL(rotationx(QGenericMatrix<3,3,float>)),threadDisplayTimer.pave,SLOT(rotation(QGenericMatrix<3,3,float>)));
    QObject::connect(threadDisplayTimer.w->rySlider,SIGNAL(valueChanged(int)),&threadBouton,SLOT(rotationySL(int)));
    QObject::connect(&threadBouton,SIGNAL(rotationy(QGenericMatrix<3,3,float>)),threadDisplayTimer.pave,SLOT(rotation(QGenericMatrix<3,3,float>)));
    QObject::connect(threadDisplayTimer.w->rzSlider,SIGNAL(valueChanged(int)),&threadBouton,SLOT(rotationzSL(int)));
    QObject::connect(&threadBouton,SIGNAL(rotationz(QGenericMatrix<3,3,float>)),threadDisplayTimer.pave,SLOT(rotation(QGenericMatrix<3,3,float>)));

    QObject::connect(threadDisplayTimer.w->zoomSlider,SIGNAL(valueChanged(int)),&threadBouton,SLOT(zoomSL(int)));
    QObject::connect(threadDisplayTimer.mTimer,SIGNAL(timeout()),threadDisplayTimer.pave,SLOT(handleTimer()));

    // Lancement du timer
    threadDisplayTimer.start();

    return app.exec();
}
