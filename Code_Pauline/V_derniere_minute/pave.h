#ifndef PAVE_H
#define PAVE_H

#include <QColor>
#include <QGenericMatrix>
#include <QObject>

#include "objetgeo.h"
#include "point3d.h"
#include "surchargesoperateurs.h"

class Pave : public ObjetGeo
{
    Q_OBJECT

public:
    // Constructeurs
    Pave();
    Pave(Point3d newCoin, Point3d newLongueur, Point3d newLargeur, Point3d newHauteur, QColor newCouleur);
    Pave(Pave & copiePave);
    Point3d getCoin(){return coin;};
    Point3d getLongueur(){return l;};
    Point3d getLargeur(){return L;};
    Point3d getHauteur(){return h;};
    ~Pave();

    // Méthodes
    Facette *getFacettes();
    // NB : il est impossible de renvoyer un tableau, sauf s'il est alloué dynamiquement.

public slots:
    void handleTimer();
    void translation(Point3d pointFin);
    void rotation(QGenericMatrix<3,3,float> R);
private:
    // Méthodes
    Facette * creeFacettes();
    Facette * triFacettes(Facette *lstFacettes);

    // Attributs
    // Caractéristiques géométriques (coin, longueur, largeur et hauteur)
    // NB : Les longueurs, largeurs et hauteurs sont vues comme les extrémités de trois vecteurs
    // pris dans la base du "coin".
    Point3d coin, l, L, h;
    QColor couleur;

};

#endif // PAVE_H
