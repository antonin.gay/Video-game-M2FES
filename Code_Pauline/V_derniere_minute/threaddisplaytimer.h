#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <iostream>
#include <QApplication>
#include <QThread>
#include <QTimer>

#include "pave.h"
#include "window.h"

class ThreadDisplayTimer : public QThread
{
    Q_OBJECT

public:
    ThreadDisplayTimer();

    QTimer *mTimer;
    Pave *pave;
    Window *w;
signals:
   void startTimer();
public slots:
    void doIt();
    void timerStart();
protected:
    void run();
private:
};

#endif  /* MYTHREAD_H */
