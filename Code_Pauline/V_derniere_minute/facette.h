#ifndef FACETTE_H
#define FACETTE_H

#include <QPolygon>
#include <QPoint>
#include <QColor>
#include <QObject>

#include "point3d.h"

class Facette
{

public:
    Facette(); // constructeur par défaut
    Facette(Point3d point1, Point3d point2, Point3d point3, QColor couleur); // constructeur explicite
    Point3d getPointA() { return pointA; }; // récupération du point A
    Point3d getPointB() { return pointB; }; // récupération du point B
    Point3d getPointC() { return pointC; }; // récupération du le point C
    // Récupération du polygone représentant la facette après application d'un facteur d'échelle
    QPolygon getPoints(float widthScaleFactor=1,float heightScaleFactor=1);
    Point3d getBarycentre(); // renvoi du barycentre de la facette
    QColor getColor(); // renvoi de la couleur de la facette
    void setPoints(Point3d A,Point3d B, Point3d C); // modification des points de la facette
    void setColor(QColor); // modification de la couleur de la facette
    // Surcharge d'opérateur '=' utile pour le tri des facettes
    Facette & operator=(const Facette & autreFacette);

private:
    Point3d pointA;
    Point3d pointB;
    Point3d pointC;
    QColor color;
};

#endif // FACETTE_H
