#include "drawingarea.h"
#include "window.h"

Window::Window()
{
    // Taille par défaut de la fenêtre
    this->setGeometry(50,100,700,800);

    // Zone de dessin
    drawingArea = new DrawingArea;

    // Sliders pour la translation et labels associés
    xSlider=new QSlider(Qt::Horizontal,this);
    xLabel=new QLabel(tr("x"));
    xLabel->setBuddy(xSlider);

    ySlider=new QSlider(Qt::Horizontal,this);
    yLabel=new QLabel(tr("y"));
    yLabel->setBuddy(ySlider);

    zSlider=new QSlider(Qt::Horizontal,this);
    zLabel=new QLabel(tr("z"));
    zLabel->setBuddy(zSlider);

    // Sliders pour la rotation et labels associés
    rxSlider=new QSlider(Qt::Horizontal,this);
    rxLabel=new QLabel(tr("Rx"));
    rxLabel->setBuddy(rxSlider);

    rySlider=new QSlider(Qt::Horizontal,this);
    ryLabel=new QLabel(tr("Ry"));
    ryLabel->setBuddy(rySlider);

    rzSlider=new QSlider(Qt::Horizontal,this);
    rzLabel=new QLabel(tr("Rz"));
    rzLabel->setBuddy(rzSlider);

    // Slider pour le zoom et label associé
    zoomSlider=new QSlider(Qt::Horizontal,this);
    zoomLabel=new QLabel(tr("Zoom"));
    zoomLabel->setBuddy(zoomSlider);

    // Structuration en layout
    QGridLayout *windowLayout = new QGridLayout;

    // Taille zones image/sliders/labels
    windowLayout->setColumnStretch(0,60);
    windowLayout->setColumnStretch(1,20);
    windowLayout->setColumnStretch(2,5);

    // Positionnement de la zone de dessin
    windowLayout->addWidget(drawingArea, 0, 0, 7, 1);

    // Positionnement des sliders
    windowLayout->addWidget(xSlider,0,1);
    windowLayout->addWidget(ySlider,1,1);
    windowLayout->addWidget(zSlider,2,1);
    windowLayout->addWidget(rxSlider,3,1);
    windowLayout->addWidget(rySlider,4,1);
    windowLayout->addWidget(rzSlider,5,1);
    windowLayout->addWidget(zoomSlider,6,1);

    // Positionnement des labels
    windowLayout->addWidget(xLabel,0,2);
    windowLayout->addWidget(yLabel,1,2);
    windowLayout->addWidget(zLabel,2,2);
    windowLayout->addWidget(rxLabel,3,2);
    windowLayout->addWidget(ryLabel,4,2);
    windowLayout->addWidget(rzLabel,5,2);
    windowLayout->addWidget(zoomLabel,6,2);
    setLayout(windowLayout);

    // Titre de la fenêtre
    setWindowTitle(tr("Visualisation 3D"));
}

Window& Window::operator=(Window& nw)
{
    // Taille par défaut de la fenêtre
    this->setGeometry(50,100,700,800);

    // Zone de dessin
    drawingArea = nw.drawingArea;

    // Sliders pour la translation et labels associés
    xSlider=nw.xSlider;
    xLabel=nw.xLabel;
    xLabel->setBuddy(xSlider);

    ySlider=nw.ySlider;
    yLabel=nw.yLabel;
    yLabel->setBuddy(ySlider);

    zSlider=nw.zSlider;
    zLabel=nw.yLabel;
    zLabel->setBuddy(zSlider);

    // Sliders pour la rotation et labels associés
    rxSlider=nw.rxSlider;
    rxLabel=nw.rxLabel;
    rxLabel->setBuddy(rxSlider);

    rySlider=nw.rySlider;
    ryLabel=nw.ryLabel;
    ryLabel->setBuddy(rySlider);

    rzSlider=nw.rzSlider;
    rzLabel=nw.rzLabel;
    rzLabel->setBuddy(rzSlider);

    // Slider pour le zoom et label associé
    zoomSlider=nw.zoomSlider;
    zoomLabel=nw.zoomLabel;
    zoomLabel->setBuddy(zoomSlider);

    // Structuration en layout
    QGridLayout *windowLayout = new QGridLayout;

    // Taille zones image/sliders/labels
    windowLayout->setColumnStretch(0,60);
    windowLayout->setColumnStretch(1,20);
    windowLayout->setColumnStretch(2,5);

    // Positionnement de la zone de dessin
    windowLayout->addWidget(drawingArea, 0, 0, 7, 1);

    // Positionnement des sliders
    windowLayout->addWidget(xSlider,0,1);
    windowLayout->addWidget(ySlider,1,1);
    windowLayout->addWidget(zSlider,2,1);
    windowLayout->addWidget(rxSlider,3,1);
    windowLayout->addWidget(rySlider,4,1);
    windowLayout->addWidget(rzSlider,5,1);
    windowLayout->addWidget(zoomSlider,6,1);

    // Positionnement des labels
    windowLayout->addWidget(xLabel,0,2);
    windowLayout->addWidget(yLabel,1,2);
    windowLayout->addWidget(zLabel,2,2);
    windowLayout->addWidget(rxLabel,3,2);
    windowLayout->addWidget(ryLabel,4,2);
    windowLayout->addWidget(rzLabel,5,2);
    windowLayout->addWidget(zoomLabel,6,2);
    setLayout(windowLayout);

    // Titre de la fenêtre
    setWindowTitle(tr("Visualisation 3D"));

    return *this;
}
