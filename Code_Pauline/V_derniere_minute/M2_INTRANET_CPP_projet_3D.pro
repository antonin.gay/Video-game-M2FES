QT += widgets

HEADERS       = drawingarea.h \
    facette.h \
    point3d.h \
    threadbouton.h \
    window.h \
    objetgeo.h \
    pave.h \
    threaddisplaytimer.h \
    surchargesoperateurs.h
SOURCES       = drawingarea.cpp \
    facette.cpp \
    main.cpp \
    point3d.cpp \
    threadbouton.cpp \
    window.cpp \
    objetgeo.cpp \
    pave.cpp \
    threaddisplaytimer.cpp \
    surchargesoperateurs.cpp
