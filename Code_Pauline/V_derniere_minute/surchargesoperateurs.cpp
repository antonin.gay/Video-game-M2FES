#include "surchargesoperateurs.h"

Point3d operator+(const Point3d& lhs, const Point3d& rhs)
{
    Point3d res;
    res.setX(lhs.getX()+rhs.getX());
    res.setY(lhs.getY()+rhs.getY());
    res.setZ(lhs.getZ()+rhs.getZ());

    return res;
}
