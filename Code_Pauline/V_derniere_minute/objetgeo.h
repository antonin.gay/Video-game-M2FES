#ifndef OBJETGEO_H
#define OBJETGEO_H

#include <QGenericMatrix>
#include <QObject>

#include "facette.h"
#include "point3d.h"

class ObjetGeo : public QObject
{
    Q_OBJECT

public:
    explicit ObjetGeo();
    virtual Facette *getFacettes()=0; // méthode virtuelle pure
    virtual ~ObjetGeo();

public slots:
    virtual void handleTimer()=0; // méthode virtuelle pure
    virtual void translation(Point3d pointFin)=0; // méthode virtuelle pure
    virtual void rotation(QGenericMatrix<3,3,float> R)=0;

private:
    virtual Facette *triFacettes(Facette *lstFacettes)=0;
};

#endif // OBJETGEO_H
