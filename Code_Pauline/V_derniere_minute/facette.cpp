#include "facette.h"

// Constructeur par défaut
Facette::Facette()
{
    pointA = Point3d(100,0,0);
    pointB = Point3d(0,100,0);
    pointC = Point3d(0,0,100);
    color = QColor(255,127,0);
}

// Constructeur explicite
Facette::Facette(Point3d point1, Point3d point2, Point3d point3, QColor couleur)
{
    pointA=point1;
    pointB=point2;
    pointC=point3;
    color=couleur;
}

// Récupération du polygone représentant la facette après application d'un facteur d'échelle
QPolygon Facette::getPoints(float widthScaleFactor,float heightScaleFactor)
{
    QPoint A(pointA.getX()*widthScaleFactor,pointA.getY()*heightScaleFactor);
    QPoint B(pointB.getX()*widthScaleFactor,pointB.getY()*heightScaleFactor);
    QPoint C(pointC.getX()*widthScaleFactor,pointC.getY()*heightScaleFactor);
    QPolygon polygon;
    polygon << A << B << C;
    return polygon;
}

// Renvoi du barycentre de la facette
Point3d Facette::getBarycentre()
{
    Point3d barycentre;
    float x;
    float y;
    float z;
    x=(pointA.getX()+pointB.getX()+pointC.getX())/3;
    y=(pointA.getY()+pointB.getY()+pointC.getY())/3;
    z=(pointA.getZ()+pointB.getZ()+pointC.getZ())/3;
    barycentre.setX(x);
    barycentre.setY(y);
    barycentre.setZ(z);
    return barycentre;
}

// Renvoi de la couleur de la facette
QColor Facette::getColor()
{
    return color;
}

// Modification des points de la facette
void Facette::setPoints(Point3d A,Point3d B, Point3d C)
{
    pointA=A;
    pointB=B;
    pointC=C;
}

// Modification de la couleur de la facette
void Facette::setColor(QColor couleur)
{
    color=couleur;
}

// Surcharge de l'opérateur "=" pour les facettes
Facette & Facette::operator=(const Facette & autreFacette){
    pointA=autreFacette.pointA;
    pointB=autreFacette.pointB;
    pointC=autreFacette.pointC;
    color=autreFacette.color;

    return *this;
}
