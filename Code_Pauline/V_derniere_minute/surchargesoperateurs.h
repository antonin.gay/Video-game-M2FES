#ifndef SURCHARGESOPERATEURS_H
#define SURCHARGESOPERATEURS_H

#include "point3d.h"

Point3d operator+(const Point3d& lhs, const Point3d& rhs);

#endif // SURCHARGESOPERATEURS_H
