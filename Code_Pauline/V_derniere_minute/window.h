#ifndef WINDOW_H
#define WINDOW_H

#include <QGridLayout>
#include <QLabel>
#include <QObject>
#include <QSlider>
#include <QWidget>

#include "drawingarea.h"

class Window : public QWidget
{
    Q_OBJECT

public:
    Window();

    DrawingArea *drawingArea;

    QSlider *xSlider;
    QSlider *ySlider;
    QSlider *zSlider;
    QSlider *rxSlider;
    QSlider *rySlider;
    QSlider *rzSlider;
    QSlider *zoomSlider;

    Window& operator=(Window& nw);

private:

    QLabel *xLabel;
    QLabel *yLabel;
    QLabel *zLabel;
    QLabel *rxLabel;
    QLabel *ryLabel;
    QLabel *rzLabel;
    QLabel *zoomLabel;

};

#endif // WINDOW_H
