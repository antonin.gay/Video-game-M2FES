#ifndef THREADBOUTON_H
#define THREADBOUTON_H

#include <QGenericMatrix>
#include <QObject>
#include <QThread>

#include "pave.h"
#include "point3d.h"
#include "threaddisplaytimer.h"

class ThreadBouton : public QThread
{
    Q_OBJECT

public:
    ThreadBouton(Window *nw,Pave *p);
    ~ThreadBouton();

    Window *w;
    Pave *pave;

protected:
    void run();

public slots:
    // Translations
    void translationxSL(int tx);
    void translationySL(int ty);
    void translationzSL(int tz);
    // Rotations
    void rotationxSL(int rx);
    void rotationySL(int ry);
    void rotationzSL(int rz);
    // Zoom
    void zoomSL(int tz);

signals:
    // Translations
    void translationx(Point3d P);
    void translationy(Point3d P);
    void translationz(Point3d P);
    // Rotations
    void rotationx(QGenericMatrix<3,3,float> R);
    void rotationy(QGenericMatrix<3,3,float> R);
    void rotationz(QGenericMatrix<3,3,float> R);

private:
    int mtx=0;
    int mty=0;
    int mtz=0;
    int mzoom=0;
};

#endif // THREADBOUTON_H
