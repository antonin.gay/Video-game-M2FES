#ifndef POINT3D_H
#define POINT3D_H

#include <QObject>

class Point3d
{
public:
    Point3d();
    Point3d(float a, float b, float c);
    float getX()const{return x;};
    float getY()const{return y;};
    float getZ()const{return z;};
    void setX(float a){x=a;};
    void setY(float a){y=a;};
    void setZ(float a){z=a;};
    Point3d& operator=(const Point3d &pointC);

private:
    float x;
    float y;
    float z;

};

#endif // POINT3D_H
