#include "threaddisplaytimer.h"

using namespace std;

ThreadDisplayTimer::ThreadDisplayTimer() {
    // Timer
    mTimer = new QTimer(this);
    connect(this,SIGNAL(startTimer()),this, SLOT(timerStart()));
    connect(mTimer,SIGNAL(timeout()),this,SLOT(doIt()));

    // Création d'un pavé
    pave = new Pave();
    // Création de la fenêtre graphique
    w = new Window();

    w->show();
    // Récupération de la liste des facettes correspondant au pavé
    Facette *facettes = new Facette[12];
    facettes = pave->getFacettes();
    // Affichage des facettes
    w->drawingArea->setFacetsToDraw(facettes,12);

    float zScaleFactor=1;
    //float zScaleFactor=1/(facettes[11].getBarycentre().getZ()/10);

    w->xSlider->setMinimum(pave->getCoin().getX());
    w->xSlider->setMaximum(w->drawingArea->defaultWidth-pave->getLargeur().getY());

    w->ySlider->setMinimum(pave->getCoin().getY());
    w->ySlider->setMaximum(w->drawingArea->defaultHeight-(pave->getLargeur().getY()));

    connect(mTimer, SIGNAL(timeout()),w,SLOT(update()));
}

void ThreadDisplayTimer::run() {
    emit(startTimer());
    exec();
}

void ThreadDisplayTimer::doIt(){
    // Récupération de la liste des facettes correspondant au pavé
    Facette *facettes;
    facettes = pave->getFacettes();
    // Affichage des facettes
    w->drawingArea->setFacetsToDraw(facettes,12);
}

void ThreadDisplayTimer::timerStart(){
    mTimer->start(100);
}
