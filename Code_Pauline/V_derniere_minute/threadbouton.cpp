#include "threadbouton.h"

ThreadBouton::ThreadBouton(Window *nw,Pave *p)
{
    w=nw;
    pave=p;

}

void ThreadBouton::run()
{
}


void ThreadBouton::translationxSL(int tx)
{
    // Translation horizontale associée à un slider
    // Entrée : Valeur du slider (entier)
    // Sortie : Variation de point nécessaire (Point3d)
    float x;
    x=tx-mtx;
    mtx=tx;
    Point3d P = Point3d(x,0,0);
    emit translationx(P);
}

void ThreadBouton::translationySL(int ty)
{
    // Translation verticale associée à un slider
    // Entrée : Valeur du slider (entier)
    // Sortie : Variation de point nécessaire (Point3d)
    float y;
    y=ty-mty;
    mty=ty;
    Point3d P = Point3d(0,y,0);
    emit translationy(P);
}

void ThreadBouton::translationzSL(int tz)
{
    // Translation en profondeur associée à un slider
    // Entrée : Valeur du slider (entier)
    // Sortie : Variation de point nécessaire (Point3d)
    float z;
    z=tz-mtz;
    mtz=tz;

    //w->xSlider->setMinimum(pave->getCoin().getX()/z);
    //w->xSlider->setMaximum(w->drawingArea->defaultWidth-pave->getLargeur().getY()/tz);

    //w->ySlider->setMinimum(pave->getCoin().getY()/z);
    //w->ySlider->setMaximum(w->drawingArea->defaultHeight-pave->getLargeur().getY()/tz);

    Point3d P = Point3d(0,0,z);
    emit translationz(P);
}

void ThreadBouton::zoomSL(int tz)
{
    // Translation en profondeur associée à un slider
    // Entrée : Valeur du slider (entier)
    // Sortie : Variation de point nécessaire (Point3d)
    float z;
    z=mzoom-tz;
    mzoom=tz;

    //w->xSlider->setMinimum(pave->getCoin().getX()/z);
    //w->xSlider->setMaximum(w->drawingArea->defaultWidth-pave->getLargeur().getY()/tz);

    //w->ySlider->setMinimum(pave->getCoin().getY()/z);
    //w->ySlider->setMaximum(w->drawingArea->defaultHeight-pave->getLargeur().getY()/tz);

    Point3d P = Point3d(0,0,z);
    emit translationz(P);
}

void ThreadBouton::rotationxSL(int rx)
{
    // Rotation selon l'axe horizontal associée à un slider
    // Entrée : Valeur du slider (entier)
    // Sortie : Matrice de rotation (QMatrix)
    float r= 2*3.14/480*rx;
    float values[] = {
        1, 0, 0,
        0, cos(r), sin(r),
        0, -sin(r), cos(r)
    };
    QGenericMatrix<3,3,float> R(values);
    emit rotationx(R);
}

void ThreadBouton::rotationySL(int ry)
{
    // Rotation selon l'axe vertical associée à un slider
    // Entrée : Valeur du slider (entier)
    // Sortie : Matrice de rotation (QMatrix)
    float r = 2*3.14/640*ry; // en radian
    float values[] = {
        -sin(r), 0, cos(r),
        0, 1, 0,
        cos(r), 0, sin(r)
    };
    QGenericMatrix<3,3,float> R(values);
    emit rotationy(R);
}

void ThreadBouton::rotationzSL(int rz)
{
    // Rotation selon l'axe vertical associée à un slider
    // Entrée : Valeur du slider (entier)
    // Sortie : Matrice de rotation (QMatrix)
    float r = rz; // en radian
    float values[] = {
        cos(r), sin(r), 0,
        sin(r), cos(r), 0,
        0, 0, 1
    };
    QGenericMatrix<3,3,float> R(values);
    emit rotationz(R);
}

ThreadBouton::~ThreadBouton()
{
}
