#ifndef DRAWINGAREA_H
#define DRAWINGAREA_H

#include <QBrush>
#include <QObject>
#include <QPen>
#include <QPixmap>
#include <QWidget>

#include "facette.h"

class DrawingArea : public QWidget
{
    Q_OBJECT

    friend class ThreadDisplayTimer;
    friend class ThreadBouton;
public:
    DrawingArea(QWidget *parent = 0); // constructeur par défaut
    int getHeight();
    int getWidth();
    void setFacetsToDraw(Facette *,int); // récupération du tableau & du nombre de facettes à dessiner

protected:
    // Réécriture de la méthode paintEvent pour éviter les erreurs d'affichage
    void paintEvent(QPaintEvent *event) override;

private:
    Facette *facets; // tableau de facettes à dessiner
    int nFacets; // nombre de facettes à dessiner
    int defaultWidth; // largeur par défaut de la zone de dessin
    int defaultHeight; // hauteur par défaut de la zone de dessin
};

#endif // DRAWINGAREA_H
