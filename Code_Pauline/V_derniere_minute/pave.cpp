#include "pave.h"
#include "facette.h"
#include "surchargesoperateurs.h"

Pave::Pave() : ObjetGeo()
{
    coin = Point3d(0,0,0);
    l = Point3d(100,0,0);
    L = Point3d(0,100,0);
    h = Point3d(0,0,100);
    couleur = QColor(255,127,0);

//    float rz = 0.707;
//    float values[] = {
//        cos(rz), sin(rz), 0,
//        -sin(rz), cos(rz), 0,
//        0, 0, 1
//    };
//    QGenericMatrix<3,3,float> R(values);
//    this->rotation(R);
//    this->translation(Point3d(55,64,0));
}

Pave::~Pave(){

}


Pave::Pave(Point3d newCoin, Point3d newLongueur, Point3d newLargeur, Point3d newHauteur,QColor newCouleur){
    coin = newCoin;
    l = newLongueur;
    L = newLargeur;
    h = newHauteur;
    couleur = newCouleur;
}

Pave::Pave(Pave &copiePave){
    coin = copiePave.coin;
    l = copiePave.l;
    L = copiePave.L;
    h = copiePave.h;
    couleur = copiePave.couleur;
}

void Pave::translation(Point3d pointFin){
    coin = coin+pointFin;
//    l = l+pointFin;
//    L = L+pointFin;
//    h = h+pointFin;
}

void Pave::rotation(QGenericMatrix<3,3,float> R){
    float lx = R(0,0)*l.getX() + R(0,1)*l.getY() + R(0,2)*l.getZ();
    float ly = R(1,0)*l.getX() + R(1,1)*l.getY() + R(1,2)*l.getZ();
    float lz = R(2,0)*l.getX() + R(2,1)*l.getY() + R(2,2)*l.getZ();

    float Lx = R(0,0)*L.getX() + R(0,1)*L.getY() + R(0,2)*L.getZ();
    float Ly = R(1,0)*L.getX() + R(1,1)*L.getY() + R(1,2)*L.getZ();
    float Lz = R(2,0)*L.getX() + R(2,1)*L.getY() + R(2,2)*L.getZ();

    float hx = R(0,0)*h.getX() + R(0,1)*h.getY() + R(0,2)*h.getZ();
    float hy = R(1,0)*h.getX() + R(1,1)*h.getY() + R(1,2)*h.getZ();
    float hz = R(2,0)*h.getX() + R(2,1)*h.getY() + R(2,2)*h.getZ();

    l = Point3d(lx,ly,lz);
    L = Point3d(Lx,Ly,Lz);
    h = Point3d(hx,hy,hz);
}

void Pave::handleTimer(){
    float rz = 0.1;
    float values[] = {
        cos(rz), sin(rz), 0,
        -sin(rz), cos(rz), 0,
        0, 0, 1
    };
    QGenericMatrix<3,3,float> R(values);
    this->rotation(R);
}

Facette * Pave::creeFacettes(){
    // déclaration des 12 facettes composant le pavé
    // Allocation dynamique d'un tableau point sur 12 objets "Facette"
    Facette * tabFacettes = new Facette[12];

    tabFacettes[0].setPoints(coin,coin+h,coin+l);
    tabFacettes[1].setPoints(coin+h,coin+l,coin+l+h);

    tabFacettes[2].setPoints(coin,coin+l,coin+L+l);
    tabFacettes[3].setPoints(coin,coin+L,coin+L+l);

    tabFacettes[4].setPoints(coin,coin+h,coin+L+h);
    tabFacettes[5].setPoints(coin,coin+L,coin+L+h);

    tabFacettes[6].setPoints(coin+L,coin+L+h,coin+L+l);
    tabFacettes[7].setPoints(coin+L+h,coin+L+l,coin+L+l+h);

    tabFacettes[8].setPoints(coin+h,coin+h+l,coin+L+h);
    tabFacettes[9].setPoints(coin+h+l,coin+h+L,coin+L+l+h);

    tabFacettes[10].setPoints(coin+l,coin+l+h,coin+l+L+h);
    tabFacettes[11].setPoints(coin+l,coin+l+L,coin+L+l+h);

    for (int i=0; i<12; i++)
        tabFacettes[i].setColor(couleur);

    return tabFacettes;
}

Facette * Pave::getFacettes(){
    Facette * lstFacette;
    lstFacette = this->creeFacettes();
    lstFacette = triFacettes(lstFacette);
    return lstFacette;
}


Facette * Pave::triFacettes(Facette *lstFacettes){
    // ordonne les 12 facettes du pavé par profondeur décroissante
    float zMax, zTemp;
    Facette facetteMax;
    Facette * lstTriee = new Facette[12];

    for (int i=0; i<12; i++) // on raccourcit progressivement la liste à trier
    {
        zMax = 0; // valeur par défaut
        facetteMax = lstFacettes[i]; // valeur par défaut
        for (int j=i; j<12; j++) // on récupère le zMax de chaque sous-liste (lstFacettes depuis l'indice i jusqu'à 12)
        {
            zTemp = lstFacettes[j].getBarycentre().getZ();
            if (zTemp > zMax)
            {
                zMax = zTemp;
                facetteMax = lstFacettes[j];
            }
        }
        lstTriee[i] = facetteMax; // à ce stade, on a pu trouver le zMax de la sous-liste en cours, et la facette associée.
    }

    return lstTriee;
}
