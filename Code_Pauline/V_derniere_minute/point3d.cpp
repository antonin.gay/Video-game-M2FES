#include "point3d.h"

// Constructeur par défaut
Point3d::Point3d()
{
    // on crée le point (0,0,0) origine
    x=0;
    y=0;
    z=0;
}

Point3d::Point3d(float a, float b, float c)
{
    //Création du point de coordonées (a,b,c)
    x=a;
    y=b;
    z=c;
}


Point3d& Point3d::operator=(const Point3d& pointC)
{
    x=pointC.getX();
    y=pointC.getY();
    z=pointC.getZ();

    return *this;
}
