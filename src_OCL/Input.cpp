//
// Created by Antonin on 26/03/2018.
//

#include "Input.h"

Input::Input() : m_x(0), m_y(0), m_xRel(0), m_yRel(0), m_terminer(false)
{

    // Initialisation du tableau m_touches
    for (bool &m_touche : m_touches) {
        m_touche = false;
    }

    for (bool &m_touche : m_touches) {
        m_touche = false;
    }

    // Initialisation du tableau m_boutonsSouris
    for (bool &m_boutonSouris : m_boutonsSouris)
    {
        m_boutonSouris = false;
    }


}

Input::~Input() = default;

void Input::updateEvenements()
{
    // Initialisation des coordonnées relatives

    m_xRel = 0;
    m_yRel = 0;

    // Boucle d'évènements

    while (SDL_PollEvent(&m_evenement))
    {
        switch (m_evenement.type)
        {
            case SDL_KEYDOWN:
                m_touches[m_evenement.key.keysym.scancode] = true;
                break;

            case SDL_KEYUP :
                m_touches[m_evenement.key.keysym.scancode] = false;
                break;

            case SDL_MOUSEBUTTONDOWN:
                m_boutonsSouris[m_evenement.button.button] = true;
                break;

            case SDL_MOUSEBUTTONUP:
                m_boutonsSouris[m_evenement.button.button] = false;
                break;

            case SDL_MOUSEMOTION:
                m_x = m_evenement.motion.x;
                m_y = m_evenement.motion.y;

                m_xRel = m_evenement.motion.xrel;
                m_yRel = m_evenement.motion.yrel;
                break;

            case SDL_WINDOWEVENT:
                m_terminer = (m_evenement.window.event == SDL_WINDOWEVENT_CLOSE);
                break;

            default:
                break;
        }
    }
}

bool Input::terminer() const
{
    return m_terminer;
}

bool Input::getTouche(const SDL_Scancode touche) const {
    return m_touches[touche];
}

bool Input::getBoutonSouris(const Uint8 bouton) const {
    return m_boutonsSouris[bouton];

}

bool Input::mouvementSouris() const {
    return (m_xRel || m_yRel);
}

int Input::getM_x() const {
    return m_x;
}

int Input::getM_y() const {
    return m_y;
}

int Input::getM_xRel() const {
    return m_xRel;
}

int Input::getM_yRel() const {
    return m_yRel;
}

void Input::afficherPointeur(bool reponse) const {
    SDL_ShowCursor((reponse) ? SDL_ENABLE : SDL_DISABLE);
}

void Input::capturerPointeur(bool reponse) const {
    SDL_SetRelativeMouseMode((reponse) ? SDL_TRUE : SDL_FALSE);
}
