//
// Created by Antonin on 27/03/2018.
//

#ifndef OPCL_CLION_PLAN_H
#define OPCL_CLION_PLAN_H

#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPES 1
#include <GL3/gl3.h>

#endif

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Defines

#define NORM_X 0
#define NORM_Y 1
#define NORM_Z 2

// Includes

#include "Shader.h"

// Macro utile au VBO

#ifndef BUFFER_OFFSET

#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))

#endif

class Carre {

public:
    Carre(int orientation, float taille, std::string vertexShader, std::string fragmentShader);

    ~Carre();

    void afficher(glm::mat4 &projection, glm::mat4 &modelview);

    virtual void charger();

    void updateBuffer(void *donnees, int tailleBytes, int decalage);

protected:
    // Attributs
    Shader m_shader;
    float m_vertices[108];
    float m_couleurs[108];

    GLuint m_vboID;
    GLuint m_vaoID;
    int m_tailleVerticesBytes;
    int m_tailleCouleursBytes;


};


#endif //OPCL_CLION_PLAN_H
