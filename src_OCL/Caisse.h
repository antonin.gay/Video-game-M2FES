//
// Created by Antonin on 26/03/2018.
//

#ifndef OPCL_CLION_CAISSE_H
#define OPCL_CLION_CAISSE_H

// Includes

#include "Cube.h"
#include "Texture.h"
#include <string>
#include <glm/glm.hpp>


// Classe Caisse

class Caisse : public Cube
{
public:

    Caisse(float taille, std::string vertexShader, std::string fragmentShader, std::string texture);
    ~Caisse();

    void afficher(glm::mat4 &projection, glm::mat4 &modelview);

    void afficher(glm::mat4 &projection, glm::mat4 &modelview, glm::vec3 translation) override;

    void charger() override;
private:

    Texture m_texture;
    float m_coordTexture[72];

    int m_tailleCoordTextureBytes;
};

#endif
