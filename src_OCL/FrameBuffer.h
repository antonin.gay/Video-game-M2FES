//
// Created by Antonin on 30/03/2018.
//

#ifndef OPCL_CLION_FRAMEBUFFER_H
#define OPCL_CLION_FRAMEBUFFER_H

// Include Windows
#ifdef WIN32
#include <GL/glew.h>
#include <vector>
#include "Texture.h"

// Include Mac
#elif __APPLE__
#define GL3_PROTOTYPES 1
#include <OpenGL/gl3.h>

// Include UNIX/Linux
#else
#define GL3_PROTOTYPES 1
#include <GL3/gl3.h>

#endif

// Classe FrameBuffer
class FrameBuffer
{
public:

    FrameBuffer();

    FrameBuffer(float m_largeur, float m_hauteur);

    virtual ~FrameBuffer();

    void creerRenderBuffer(GLuint &id, GLenum formatInterne);

    bool charger();

    float getLargeur() const;

    float getHauteur() const;

    GLuint getID() const;

    GLuint getColorBufferID(unsigned int index) const;

private:

    //Attributs
    GLuint m_id;
    float m_largeur;
    float m_hauteur;
    std::vector<Texture> m_colorBuffers;
    GLuint m_depthBufferID;

    bool configurationFBO();
};

#endif //OPCL_CLION_FRAMEBUFFER_H
