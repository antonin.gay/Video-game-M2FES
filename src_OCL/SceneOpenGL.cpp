#include "SceneOpenGL.h"

#include <utility>

using namespace glm;

// Constructeur de Destucteur

SceneOpenGL::SceneOpenGL(std::string titreFenetre, int largeurFenetre, int hauteurFenetre) :
        m_titreFenetre(std::move(titreFenetre)), m_largeurFenetre(largeurFenetre),
        m_hauteurFenetre(hauteurFenetre), m_fenetre(nullptr), m_contexteOpenGL(nullptr),
        m_input() {}


SceneOpenGL::~SceneOpenGL()
{
    SDL_GL_DeleteContext(m_contexteOpenGL);
    SDL_DestroyWindow(m_fenetre);
    SDL_Quit();
}


// Méthodes

bool SceneOpenGL::initialiserFenetre()
{
    // Initialisation de la SDL

    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << std::endl;
        SDL_Quit();

        return false;
    }

    // Version d'OpenGL
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // Double Buffer
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);


    // Création de la fenêtre

    m_fenetre = SDL_CreateWindow(m_titreFenetre.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_largeurFenetre, m_hauteurFenetre, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

    if(m_fenetre == nullptr)
    {
        std::cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << std::endl;
        SDL_Quit();

        return false;
    }


    // Création du contexte OpenGL

    m_contexteOpenGL = SDL_GL_CreateContext(m_fenetre);

    if(m_contexteOpenGL == nullptr)
    {
        std::cout << SDL_GetError() << std::endl;
        SDL_DestroyWindow(m_fenetre);
        SDL_Quit();

        return false;
    }

    return true;
}


bool SceneOpenGL::initGL()
{
#ifdef WIN32

    // On initialise GLEW

    GLenum initialisationGLEW( glewInit() );


    // Si l'initialisation a échoué :

    if(initialisationGLEW != GLEW_OK)
    {
        // On affiche l'erreur grâce à la fonction : glewGetErrorString(GLenum code)

        std::cout << "Erreur d'initialisation de GLEW : " << glewGetErrorString(initialisationGLEW) << std::endl;


        // On quitte la SDL

        SDL_GL_DeleteContext(m_contexteOpenGL);
        SDL_DestroyWindow(m_fenetre);
        SDL_Quit();

        return false;
    }

#endif

    // Activation du Depth Buffer

    glEnable(GL_DEPTH_TEST);

    // Tout s'est bien passé, on retourne true

    return true;
}


void SceneOpenGL::bouclePrincipale()
{
    // Variables
    unsigned int frameRate(16);
    Uint32 debutBoucle(0), finBoucle(0), tempsEcoule(0);

    // Matrices
    mat4 projection;
    mat4 modelview;

    // Initialisation des matrices

    projection = perspective(70.0, (double) m_largeurFenetre / m_hauteurFenetre, 1.0, 100.0);
    modelview = mat4(1.0);

    // Camera mobile

    Camera camera(vec3(3, 3, 3), vec3(0, 0, 0), vec3(0, 1, 0), 0.5f, 0.5f);

    // Cacher pointeur
    m_input.afficherPointeur(false);
    m_input.capturerPointeur(true);

    // Vertices
    float vertices[] = {4, 0, 4,   4, 0, -4,   -4, 0, -4,   // Triangle 1
                        4, 0, 4,   -4, 0, 4,   -4, 0, -4};  // Triangle 2

    // Coordonnées de texture

    float coordTexture[] = {0, 0,   4, 0,   4, 4,     // Triangle 1
                            0, 0,   0, 4,   4, 4};    // Triangle 2

    // Textures
    Texture texture("Assets/texturePackRealistic/photorealistic_vegetal/veg008.jpg");
    texture.charger();

    Shader shaderTexture("Shaders/texture.vert", "Shaders/texture.frag");
    shaderTexture.charger();

    // Création des objets 3D

    Caisse caisse1(1.0, "Shaders/texture.vert", "Shaders/texture.frag",
                  "Assets/texturePackRealistic/photorealistic_crate/crate13.jpg");
    caisse1.charger();

    Caisse caisse2(1.0, "Shaders/texture.vert", "Shaders/texture.frag",
                  "Assets/texturePackRealistic/photorealistic_crate/crate13.jpg");
    caisse2.charger();

    Caisse caisse3(1.0, "Shaders/texture.vert", "Shaders/texture.frag",
                  "Assets/texturePackRealistic/photorealistic_crate/crate13.jpg");
    caisse3.charger();

    Mur sol(NORM_Y, 10.0, "Shaders/texture.vert", "Shaders/texture.frag",
            "Assets/texturePackRealistic/photorealistic_vegetal/veg011.jpg", 2.0);
    sol.charger();

    Mur fond(NORM_Y, 4.0, "Shaders/texture.vert", "Shaders/texture.frag",
             "Assets/texturePackRealistic/photorealistic_wall/wall067_2.jpg", 1.0);
    fond.charger();

    // Boucle principale
    while(!m_input.terminer())
    {
        // On définit le temps de début de boucle
        debutBoucle = SDL_GetTicks();

        // Gestion des évènements
        m_input.updateEvenements();
        if(m_input.getTouche(SDL_SCANCODE_ESCAPE) || m_input.terminer())
            break;

        // Camera
        camera.deplacer(m_input);

        // Nettoyage de l'écran
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Placement de la caméra
        camera.lookAt(modelview);

        //======== Affichage des objets 3D ===========

        mat4 sauvegardeModelview = modelview;

        caisse3.afficher(projection, modelview, vec3(0, 0.5, 0));
        caisse3.afficher(projection, modelview, vec3(1.2, 0.5, 0));
        caisse3.afficher(projection, modelview, vec3(2.3, 0.5, 0));
        caisse3.afficher(projection, modelview, vec3(0.6, 1.5, 0));

            sol.afficher(projection, modelview);

        modelview = sauvegardeModelview;

            modelview = translate(modelview, vec3(0, 2.0, -3.0));
            fond.afficher(projection, modelview);


        // Activation du shader
        glUseProgram(shaderTexture.getProgramID());

            // Envoi des vertices
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, vertices);
            glEnableVertexAttribArray(0);

            // Envoi des coordonnées de texture
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, coordTexture);
            glEnableVertexAttribArray(2);

            // Envoi des matrices
            glUniformMatrix4fv(glGetUniformLocation(shaderTexture.getProgramID(), "projection"), 1, GL_FALSE, value_ptr(projection));
            glUniformMatrix4fv(glGetUniformLocation(shaderTexture.getProgramID(), "modelview"), 1, GL_FALSE, value_ptr(modelview));

            // Verrouillage de la texture
            glBindTexture(GL_TEXTURE_2D, texture.getID());

            // Rendu
            glDrawArrays(GL_TRIANGLES, 0, 6);

            // Déverrouillage de la texture
            glBindTexture(GL_TEXTURE_2D, 0);

            // Désactivation des tableaux
            glDisableVertexAttribArray(2);
            glDisableVertexAttribArray(0);

        // Désactivation du shader
        glUseProgram(0);


        // Actualisation de la fenêtre

        SDL_GL_SwapWindow(m_fenetre);


        // Calcul du temps écoulé

        finBoucle = SDL_GetTicks();
        tempsEcoule = finBoucle - debutBoucle;


        // Si nécessaire, on met en pause le programme

        if(tempsEcoule < frameRate)
            SDL_Delay(frameRate - tempsEcoule);
    }
}


void SceneOpenGL::updateRotations(float *angleX, float *angleY)
{
    // Incrémentation des angles
    if (m_input.getTouche(SDL_SCANCODE_LEFT))
        *angleY += 2.0 * M_PI * 1.0 / 360.0;
    if (m_input.getTouche(SDL_SCANCODE_RIGHT))
        *angleY -= 2.0 * M_PI * 1.0 / 360.0;

    if (m_input.getTouche(SDL_SCANCODE_UP))
        *angleX += 2.0 * M_PI * 1.0 / 360.0;
    if (m_input.getTouche(SDL_SCANCODE_DOWN))
        *angleX -= 2.0 * M_PI * 1.0 / 360.0;

    if(*angleY >= 2.0 * M_PI)
        *angleY -= 2.0 * M_PI;
    else if (angleY <= 0)
        *angleY += 2.0 * M_PI;

    if(*angleY >= 2.0 * M_PI)
        *angleY -= 2.0 * M_PI;
    else if (*angleY <= 0)
        *angleY += 2.0 * M_PI;
}