//
// Created by Antonin on 27/03/2018.
//

#include "Shader.h"

Shader::Shader() : m_vertexID(0), m_fragmentID(0), m_programID(0), m_vertexSource(), m_fragmentSource()
{

}

Shader::Shader(std::string vertexSource, std::string fragmentSource) :
        m_vertexID(0), m_fragmentID(0), m_programID(0),
        m_vertexSource(std::move(vertexSource)), m_fragmentSource(std::move(fragmentSource))
{

}

Shader::Shader(Shader const &shaderACopier)
{
    // Copie des fichiers source
    m_vertexSource = shaderACopier.m_vertexSource;
    m_fragmentSource = shaderACopier.m_fragmentSource;

    // Chargement du nouveau shader
    charger();
}

Shader &Shader::operator=(Shader const &shaderACopier)
{
    // Copie des fichiers source
    m_vertexSource = shaderACopier.m_vertexSource;
    m_fragmentSource = shaderACopier.m_fragmentSource;

    // Chargement du nouveau shader
    charger();

    return *this;
}

Shader::~Shader()
{
    // Destruction du shader
    glDeleteShader(m_vertexID);
    glDeleteShader(m_fragmentID);
    glDeleteProgram(m_programID);
}

GLuint Shader::getProgramID() const
{
    return m_programID;
}

bool Shader::charger()
{
    // Destruction d'un éventuel ancien Shader
    if(glIsShader(m_vertexID) == GL_TRUE) glDeleteShader(m_vertexID);
    if(glIsShader(m_fragmentID) == GL_TRUE) glDeleteShader(m_fragmentID);
    if(glIsProgram(m_programID) == GL_TRUE) glDeleteProgram(m_programID);

    if(!compileShader(m_vertexID, GL_VERTEX_SHADER, m_vertexSource))
        return false;

    if(!compileShader(m_fragmentID, GL_FRAGMENT_SHADER, m_fragmentSource))
        return false;

    // Création du programme
    m_programID = glCreateProgram();

    // Association des shaders
    glAttachShader(m_programID, m_vertexID);
    glAttachShader(m_programID, m_fragmentID);

    // Verrouillage des entrées shader
    glBindAttribLocation(m_programID, 0, "in_Vertex");
    glBindAttribLocation(m_programID, 1, "in_Color");
    glBindAttribLocation(m_programID, 2, "in_TexCoord0");

    // Linkage du programme
    glLinkProgram(m_programID);

    // On vérifie si il y a eu une erreur de link
    return isLinkError(m_programID);
}

bool Shader::compileShader(GLuint &shader, GLenum type, std::string const &fichierSource)
{

    // Création du shader
    shader = glCreateShader(type);

    // Vérification du shader
    if(shader == 0)
    {
        std::cout << "Erreur, le type de shader (" << type << ") n'existe pas" << std::endl;
        return false;
    }

    // Flux de lecture
    std::ifstream fichier(fichierSource.c_str());

    // Test d'ouverture
    if(!fichier)
    {
        std::cout << "Erreur le fichier " << fichierSource << " est introuvable" << std::endl;
        glDeleteShader(shader);

        return false;
    }

    // Strings permettant de lire le code source
    std::string ligne;
    std::string codeSource;

    // Lecture
    while(getline(fichier, ligne))
        codeSource += ligne + "\n";

    // Fermeture du fichier
    fichier.close();

    // Récupération de la chaine C du code source
    const GLchar *chaineCodeSource = codeSource.c_str();

    // Envoi du code source au shader
    glShaderSource(shader, 1, &chaineCodeSource, nullptr);

    // Compilation du shader
    glCompileShader(shader);

    return isCompilationError(shader);
}

bool Shader::isCompilationError(GLuint &shader)
{
    // Vérification de la compilation
    GLint erreurCompilation(0);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &erreurCompilation);

    // S'il y a eu une erreur
    if(erreurCompilation != GL_TRUE)
    {
        // Récupération de la taille de l'erreur
        GLint tailleErreur(0);
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &tailleErreur);

        // Allocation de mémoire
        auto *erreur = new char[tailleErreur + 1];

        // Récupération de l'erreur
        glGetShaderInfoLog(shader, tailleErreur, &tailleErreur, erreur);
        erreur[tailleErreur] = '\0';

        // Affichage de l'erreur
        std::cout << erreur << std::endl;

        // Libération de la mémoire et retour du booléen false
        delete[] erreur;
        glDeleteShader(shader);

        return false;
    }

        // Sinon c'est que tout s'est bien passé
    else
        return true;
}

bool Shader::isLinkError(GLuint &programID)
{
    // Vérification du linkage
    GLint erreurLink(0);
    glGetProgramiv(programID, GL_LINK_STATUS, &erreurLink);

    // S'il y a eu une erreur
    if(erreurLink != GL_TRUE)
    {
        // Récupération de la taille de l'erreur
        GLint tailleErreur(0);
        glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &tailleErreur);

        // Allocation de mémoire
        auto *erreur = new char[tailleErreur + 1];

        // Récupération de l'erreur
        glGetShaderInfoLog(programID, tailleErreur, &tailleErreur, erreur);
        erreur[tailleErreur] = '\0';


        // Affichage de l'erreur
        std::cout << erreur << std::endl;

        // Libération de la mémoire et retour du booléen false
        delete[] erreur;
        glDeleteProgram(programID);

        return false;
    }

        // Sinon c'est que tout s'est bien passé
    else
        return true;
}

void Shader::envoyerMat4(std::string nom, glm::mat4 matrice)
{
    int location = glGetUniformLocation(getProgramID(), nom.c_str());
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrice));

}




