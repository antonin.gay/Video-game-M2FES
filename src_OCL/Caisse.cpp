//
// Created by Antonin on 26/03/2018.
//

#include "Caisse.h"

Caisse::Caisse(float taille, std::string const vertexShader, std::string const fragmentShader,
               std::string const texture) : Cube(taille, vertexShader, fragmentShader), m_texture(texture),
                                            m_tailleCoordTextureBytes(72 * sizeof(float))
{

    m_texture.charger();

    // Coordonnées de texture temporaires

    float coordTextureTmp[] = {0, 0, 1, 0, 1, 1,     // Face 1
                               0, 0, 0, 1, 1, 1,     // Face 1

                               0, 0, 1, 0, 1, 1,     // Face 2
                               0, 0, 0, 1, 1, 1,     // Face 2

                               0, 0, 1, 0, 1, 1,     // Face 3
                               0, 0, 0, 1, 1, 1,     // Face 3

                               0, 0, 1, 0, 1, 1,     // Face 4
                               0, 0, 0, 1, 1, 1,     // Face 4

                               0, 0, 1, 0, 1, 1,     // Face 5
                               0, 0, 0, 1, 1, 1,     // Face 5

                               0, 0, 1, 0, 1, 1,     // Face 6
                               0, 0, 0, 1, 1, 1};    // Face 6


    // Copie des valeurs dans le tableau final

    for (int i(0); i < 72; i++)
        m_coordTexture[i] = coordTextureTmp[i];
}

Caisse::~Caisse() = default;

void Caisse::afficher(glm::mat4 &projection, glm::mat4 &modelview)
{
    // Activation du shader

    glUseProgram(m_shader.getProgramID());

        // Verrouillage VAO
        glBindVertexArray(m_vaoID);

            // Verrouillage de la texture
            glBindTexture(GL_TEXTURE_2D, m_texture.getID());

                // Envoi des matrices
                m_shader.envoyerMat4("modelviewProjection", projection * modelview);

                // Rendu
                glDrawArrays(GL_TRIANGLES, 0, 36);

            // Déverrouillage de la texture
            glBindTexture(GL_TEXTURE_2D, 0);

        // Déverrouillage VAO
        glBindVertexArray(0);

    // Désactivation du shader
    glUseProgram(0);
}


void Caisse::charger() {

    if(glIsBuffer(m_vboID) == GL_TRUE)
        glDeleteBuffers(1, &m_vboID);

    // Génération de l'ID
    glGenBuffers(1, &m_vboID);

    // Verrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

        // Allocation de la mémoire
        glBufferData(GL_ARRAY_BUFFER, m_tailleVerticesBytes + m_tailleCoordTextureBytes, nullptr, GL_STATIC_DRAW);

        // Transfert des données
        glBufferSubData(GL_ARRAY_BUFFER, 0, m_tailleVerticesBytes, m_vertices);
        glBufferSubData(GL_ARRAY_BUFFER, m_tailleVerticesBytes, m_tailleCoordTextureBytes, m_coordTexture);

    // Déverrouillage de l'objet
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    /* =======
     *  VAO
     ========= */

    if(glIsVertexArray(m_vaoID) == GL_TRUE)
        glDeleteVertexArrays(1, &m_vaoID);

    // Génération de l'ID
    glGenVertexArrays(1, &m_vaoID);

    // Verrouillage du V1O
    glBindVertexArray(m_vaoID);

        // Verrouillage VBO
        glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

            // Envoi des vertices
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
            glEnableVertexAttribArray(0);

            // Envoi des coordonnées de texture
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(m_tailleVerticesBytes));
            glEnableVertexAttribArray(2);

        // Déverrouillage VBO
        glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Déverrouillage de l'objet
    glBindVertexArray(0);
}

void Caisse::afficher(glm::mat4 &projection, glm::mat4 &modelview, glm::vec3 translation) {
    glm::mat4 sauvegardeModelview = modelview;

    modelview = translate(modelview, translation);
    afficher(projection, modelview);

    modelview = sauvegardeModelview;
}
