//
// Created by Antonin on 27/03/2018.
//

#include "Mur.h"

Mur::Mur(int orientation, float taille, std::string vertexShader, std::string fragmentShader,
         std::string texture, int tailletexture) :
        Carre(orientation, taille, vertexShader, fragmentShader),
        m_texture(texture), m_tailleCoordTextureBytes(12 * sizeof(float)) {
    m_texture.charger();

    // Coordonnees de textures temporaires
    float coord = taille / tailletexture;

    float coordTextureTmp[] = {coord, coord, coord, 0, 0, coord,     // Face 1
                               0, 0, 0, coord, coord, 0};    // Face 1

    // Copie des valeurs dans le tableau final
    for (int i(0); i < 12; i++)
        m_coordTexture[i] = coordTextureTmp[i];

}


Mur::~Mur() = default;

void Mur::afficher(glm::mat4 &projection, glm::mat4 &modelview)
{
    // Activation du shader
    glUseProgram(m_shader.getProgramID());

        // Verrouillage VAO
        glBindVertexArray(m_vaoID);

            // Envoi des matrices
            glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelviewProjection"), 1, GL_FALSE, value_ptr(projection * modelview));

            // Verrouillage de la texture
            glBindTexture(GL_TEXTURE_2D, m_texture.getID());

                // Rendu
                glDrawArrays(GL_TRIANGLES, 0, 6);

            // Déverrouillage de la texture
            glBindTexture(GL_TEXTURE_2D, 0);

        // Déverrouillage VAO
        glBindVertexArray(0);

    // Désactivation du shader
    glUseProgram(0);
}

void Mur::charger()
{
    if(glIsBuffer(m_vboID) == GL_TRUE)
        glDeleteBuffers(1, &m_vboID);

    // Génération de l'ID
    glGenBuffers(1, &m_vboID);

    // Verrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

        // Allocation de la mémoire
        glBufferData(GL_ARRAY_BUFFER, m_tailleVerticesBytes + m_tailleCoordTextureBytes, nullptr, GL_STATIC_DRAW);

        // Transfert des données
        glBufferSubData(GL_ARRAY_BUFFER, 0, m_tailleVerticesBytes, m_vertices);
        glBufferSubData(GL_ARRAY_BUFFER, m_tailleVerticesBytes, m_tailleCoordTextureBytes, m_coordTexture);

    // Déverrouillage de l'objet
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    /* =======
     *  VAO
     ========= */

    if(glIsVertexArray(m_vaoID) == GL_TRUE)
        glDeleteVertexArrays(1, &m_vaoID);

    // Génération de l'ID
    glGenVertexArrays(1, &m_vaoID);

    // Verrouillage du V1O
    glBindVertexArray(m_vaoID);

    // Verrouillage VBO
        glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

            // Envoi des vertices
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
            glEnableVertexAttribArray(0);

            // Envoi des coordonnées de texture
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(m_tailleVerticesBytes));
            glEnableVertexAttribArray(2);

        // Déverrouillage VBO
        glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Déverrouillage de l'objet
    glBindVertexArray(0);

}
